package com.sdskapps.circle;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import org.polaric.colorful.Colorful;

/**
 * Created by Shubhankar on 8/7/2017.
 */

public class Circle extends View {
    private static final int START_ANGLE_POINT = 270;
    private int color= Colorful.getThemeDelegate().getPrimaryColor().getColorRes();
    private final Paint paint;
    private final RectF rect;

    private float angle;

    public Circle(Context context, AttributeSet attrs) {
        super(context, attrs);

        final int strokeWidth = 20;

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(strokeWidth);
        //Circle color
       // paint.setColor(getResources().getColor(R.color.colorPrimary));
        paint.setColor(getResources().getColor(color));
        //size 200x200 example
     //   rect = new RectF(strokeWidth, strokeWidth, 500 + strokeWidth, 500 + strokeWidth);
        rect = new RectF();
        //Initial Angle (optional, it can be zero)
        angle = 0;
    }

    @Override
    protected void onDraw(Canvas canvas) {
       // Bitmap bitmap = Bitmap.createBitmap(100,100, Bitmap.Config.ARGB_8888);
       // canvas = new Canvas(bitmap);
        super.onDraw(canvas);
        int height=canvas.getHeight()/5;
        int width=canvas.getWidth()/2;
        rect.set(width - 250, height - 250, width + 250, height + 250);
        //canvas.drawCircle(getWidth()/2, getHeight()/2, 200, paint);
        canvas.drawArc(rect, START_ANGLE_POINT, angle, false, paint);
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }
}
