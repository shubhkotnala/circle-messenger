package com.sdskapps.circle;


import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.polaric.colorful.Colorful;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChatsFragment extends Fragment {
    private int primary= Colorful.getThemeDelegate().getPrimaryColor().getColorRes();
    private RecyclerView mConvList;

    private DatabaseReference mConvDatabase;
    private DatabaseReference mMessageDatabase;
    private DatabaseReference mUsersDatabase;

    private FirebaseAuth mAuth;

    private String mCurrent_user_id;

    private View mMainView;



    public ChatsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mMainView = inflater.inflate(R.layout.fragment_chats, container, false);

        mConvList = (RecyclerView) mMainView.findViewById(R.id.conv_list);
        mAuth = FirebaseAuth.getInstance();

        mCurrent_user_id = mAuth.getCurrentUser().getUid();

        mConvDatabase = FirebaseDatabase.getInstance().getReference().child("Chat").child(mCurrent_user_id);

        mConvDatabase.keepSynced(true);
        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mMessageDatabase = FirebaseDatabase.getInstance().getReference().child("messages").child(mCurrent_user_id);
        mMessageDatabase.keepSynced(true);
        mUsersDatabase.keepSynced(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        mConvList.setHasFixedSize(true);
        mConvList.setLayoutManager(linearLayoutManager);


        // Inflate the layout for this fragment
        return mMainView;
    }


    @Override
    public void onStart() {
        super.onStart();



        Query conversationQuery = mConvDatabase.orderByChild("timestamp").limitToLast(10);

        FirebaseRecyclerAdapter<Conv, ConvViewHolder> firebaseConvAdapter = new FirebaseRecyclerAdapter<Conv, ConvViewHolder>(
                Conv.class,
                R.layout.users_single_layout,
                ConvViewHolder.class,
                conversationQuery
        ) {
            @Override
            protected void populateViewHolder(final ConvViewHolder convViewHolder, final Conv conv, int i) {



                final String list_user_id = getRef(i).getKey();

                Query lastMessageQuery = mMessageDatabase.child(list_user_id).limitToLast(1);

                lastMessageQuery.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                        if(dataSnapshot.exists()){


                        String data;
                        String seen;
                        String type;
                        String from;
                        Log.d("Seen Boolean","" + conv.isSeen());
                        try{
                            data = dataSnapshot.child("message").getValue().toString();
                            seen = dataSnapshot.child("seen").getValue().toString();
                            type = dataSnapshot.child("type").getValue().toString();
                            from = dataSnapshot.child("from").getValue().toString();
                            Log.d("Seen Snapshot", seen);
                            convViewHolder.setMessage(data, seen, type,from,mCurrent_user_id);
                        }catch(Exception e){
                            data = "EXCEPTION!!!";
                            seen="false";
                            type="message";
                            dataSnapshot.getRef().removeValue();
                         //   from = dataSnapshot.child("from").getValue().toString();
                        }


                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


                mUsersDatabase.child(list_user_id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        final String userName = dataSnapshot.child("name").getValue().toString();
                        final String userThumb = dataSnapshot.child("thumb_image").getValue().toString();
                        final String status = dataSnapshot.child("status").getValue().toString();

                        if(dataSnapshot.hasChild("online")) {

                            String userOnline = dataSnapshot.child("online").getValue().toString();
                            convViewHolder.setUserOnline(userOnline);

                        }

                        convViewHolder.setName(userName);
                        convViewHolder.setUserImage(userName,userThumb, getContext());

                        convViewHolder.image.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                convViewHolder.image.setTransitionName("msgImage");
                                Intent messageImageIntent = new Intent(getContext(),MessageImageViewer.class);
                                messageImageIntent.putExtra("messageImage",userThumb);
                                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(),convViewHolder.image,"msgImage");
                                startActivity(messageImageIntent,options.toBundle());
                            }
                        });

                        convViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                convViewHolder.image.setTransitionName("displayImage");
                                convViewHolder.userNameView.setTransitionName("displayName");

                                Intent chatIntent = new Intent(getContext(), ChatActivity.class);
                                chatIntent.putExtra("user_id", list_user_id);
                                chatIntent.putExtra("user_name", userName);
                                chatIntent.putExtra("color",primary);
                                chatIntent.putExtra("status",status);

                                Pair<View, String>[] pairs = new Pair[2];
                                pairs[0] = new Pair<View,String>(convViewHolder.image,"displayImage");
                                pairs[1] = new Pair<View, String>(convViewHolder.userNameView,"displayName");

                                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(),pairs);

                                startActivity(chatIntent,options.toBundle());

                            }
                        });

                        convViewHolder.mView.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                Intent dbFixINtent = new Intent(getActivity(),DbFix.class);
                                dbFixINtent.putExtra("user_id",list_user_id);
                                startActivity(dbFixINtent);
                                return false;
                            }
                        });

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        };

      //  mConvList.getRecycledViewPool().clear();
        firebaseConvAdapter.notifyDataSetChanged();
        mConvList.setAdapter(firebaseConvAdapter);
        mConvList.getRecycledViewPool().clear();
        firebaseConvAdapter.notifyDataSetChanged();

    }

    public static class ConvViewHolder extends RecyclerView.ViewHolder {

        View mView;
        TextView userNameView;
        ImageView image;

        public ConvViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

        }

        public void setMessage(String message, String isSeen, String type, String from, String currentUser){

            TextView userStatusView = (TextView) mView.findViewById(R.id.user_single_status);
            userStatusView.setText(message+" ");
       /*     if (message.startsWith("https://firebasestorage.googleapis.com"))
            {
                userStatusView.setText("*IMAGE* ");

            } else if (message.startsWith("load.sad.gif.here")){
                userStatusView.setText("*STICKER* ");

            }  */
            if (message.startsWith("load.sad.gif.here")) {
                userStatusView.setText("*STICKER*");
            } else if (type.equals("text")) {
                userStatusView.setText(message + " ");
            } else if (type.equals("image")) {
                userStatusView.setText("*IMAGE*");
            } else if (type.startsWith("document")) {
                userStatusView.setText("*DOCUMENT*");
            }
           if(from.equals(currentUser)){
               userStatusView.setTypeface(userStatusView.getTypeface(), Typeface.NORMAL);
               userStatusView.setTextColor(Color.GRAY);
             //  RelativeLayout userLayout = (RelativeLayout) mView.findViewById(R.id.userLayout);
              // userLayout.setBackgroundColor(Color.WHITE);
           }else {


               Log.d("SeenValue", isSeen);

               if (!isSeen.equals("true")) {
                   userStatusView.setTypeface(userStatusView.getTypeface(), Typeface.BOLD);
                   userStatusView.setTextColor(Color.BLACK);
                 //  RelativeLayout userLayout = (RelativeLayout) mView.findViewById(R.id.userLayout);
                //   userLayout.setBackgroundColor(Color.LTGRAY);
                   Log.d("Seen", "Black");
               } else {
                   userStatusView.setTypeface(userStatusView.getTypeface(), Typeface.NORMAL);
                   userStatusView.setTextColor(Color.GRAY);
                  // RelativeLayout userLayout = (RelativeLayout) mView.findViewById(R.id.userLayout);
                  // userLayout.setBackgroundColor(Color.WHITE);
                   Log.d("Seen", "Gray");
               }
           }
        }

        public void setName(String name){

            userNameView = (TextView) mView.findViewById(R.id.user_single_name);
            userNameView.setText(name+" ");

        }

        public void setUserImage(String name,String thumb_image, Context ctx){


                ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
                // generate random color
                int color1 = generator.getRandomColor();

                TextDrawable drawable = TextDrawable.builder() .beginConfig()
                        .width(60)
                        .height(60)
                        .endConfig()
                        .buildRound(Character.toString(name.charAt(0)).toUpperCase(), color1);

                image = (ImageView) mView.findViewById(R.id.user_single_image);
                image.setImageDrawable(drawable);
               CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.user_single_image);
               Picasso.with(ctx).load(thumb_image).networkPolicy(NetworkPolicy.OFFLINE)
                       .placeholder(drawable).into(userImageView, new Callback() {
                   @Override
                   public void onSuccess() {

                   }

                   @Override
                   public void onError() {
                        Picasso.with(ctx).load(thumb_image).placeholder(drawable).into(userImageView);
                   }
               });





        }

        public void setUserOnline(String online_status)     {

            CircleImageView userOnlineView = (CircleImageView) mView.findViewById(R.id.user_single_online_icon);

            if(online_status.equals("true") || online_status.equals("onlineTyping")){

                userOnlineView.setVisibility(View.VISIBLE);

            } else {

                userOnlineView.setVisibility(View.INVISIBLE);

            }

        }


    }

    @Override
    public void onResume() {
        super.onResume();

    }
}