package com.sdskapps.circle;

import android.provider.ContactsContract;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostRequestData {
    @SerializedName("data")
    @Expose
    private ContactsContract.Contacts.Data data;
    @SerializedName("to")
    @Expose
    private String to;

    public ContactsContract.Contacts.Data getData() {
        return data;
    }

    public void setData(ContactsContract.Contacts.Data data) {
        this.data = data;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

}