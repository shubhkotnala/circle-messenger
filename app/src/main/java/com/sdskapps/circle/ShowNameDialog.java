package com.sdskapps.circle;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Process;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.polaric.colorful.CActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import jp.wasabeef.picasso.transformations.BlurTransformation;

public class ShowNameDialog extends CActivity {


    private ProgressDialog mRegProgress;
    private ProgressDialog progressDialog;
    private StorageReference mImageStorage;
    private DatabaseReference mDatabase;
    private static final int GALLERY_PICK = 1;
    byte[] imageByte = null;
    Uri image = null;

    private CircleImageView uImage;
    private EditText uName,uEmail,uStatus;
    private Button btnDone,btnCancel;
    private ImageView profileBg;

    private ValueEventListener mListner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_name_dialog);

      //  this.setFinishOnTouchOutside(false);

      //  getSupportActionBar().setElevation(0);
      //  mRegProgress = new ProgressDialog(this);

        Window w = getWindow(); // in Activity's onCreate() for instance
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);


        final  Transformation transformation = new BlurTransformation(getApplicationContext(),50);

        uImage = (CircleImageView) findViewById(R.id.custom_id);
        uName = (EditText) findViewById(R.id.custom_name);
        uEmail =(EditText) findViewById(R.id.custom_email);
        uStatus = (EditText) findViewById(R.id.custom_status);
        profileBg = (ImageView) findViewById(R.id.about_bg_image);

        btnDone = (Button) findViewById(R.id.btnShowNameDone);
        btnCancel = (Button) findViewById(R.id.btnShowNameCancel);

        final String phone = getIntent().getStringExtra("phone");
        final String uid = getIntent().getStringExtra("uid");

        mDatabase= FirebaseDatabase.getInstance().getReference().child("Users");
        mImageStorage = FirebaseStorage.getInstance().getReference();

       mListner = mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(uid)) {

                    Intent shownameIntent = new Intent(ShowNameDialog.this, MainActivity.class);
                    shownameIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(shownameIntent);

              //  restart();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //btnDone.setIndeterminateProgressMode(true);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String name = uName.getText().toString();
                final String email = uEmail.getText().toString();
                final String status = uStatus.getText().toString();

                if(TextUtils.isEmpty(name)){
                    uName.setError("Enter name");
                }
            /**    if (TextUtils.isEmpty(email)){

                    if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                        uEmail.setError("Enter valid email");
                    }else{
                        uEmail.setError("Enter Email");
                    }

                } */
                if (!TextUtils.isEmpty(name)
                       // && !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
                        ){

                    final String device_token = FirebaseInstanceId.getInstance().getToken();



                    if(imageByte != null) {
                        StorageReference filepath = mImageStorage.child("profile_images").child(uid + ".jpg");
                        final StorageReference thumb_Path = mImageStorage.child("profile_images").child("thumbs").child(uid + ".jpg");
                        //progress dialog
                        progressDialog = new ProgressDialog(ShowNameDialog.this);
                        progressDialog.setMax(100);
                        progressDialog.setTitle("Uploading...");
                        progressDialog.setMessage("Please wait while we compress your Profile Photo and upload it.");
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                        progressDialog.show();
                        progressDialog.setCancelable(false);
                       // UploadTask uploadTask = filepath.putBytes(imageByte);

                        filepath.putFile(image).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                                //sets and increments value of progressbar
                                progressDialog.incrementProgressBy((int) progress);
                            }
                        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot thumb_task) {
                                //final String download_url = thumb_task.getMetadata().getReference().getDownloadUrl().toString();
                                filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        String download_url = uri.toString();
                                        UploadTask thumbUpload = thumb_Path.putBytes(imageByte);
                                        progressDialog.setProgress(0);

                                        thumbUpload.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                            @Override
                                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                                                //sets and increments value of progressbar
                                                progressDialog.incrementProgressBy((int) progress);
                                            }
                                        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                            @Override
                                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                               // String thumbDownloadUrl = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
                                                    thumb_Path.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                        @Override
                                                        public void onSuccess(Uri thumb_uri) {

                                                            String thumbDownloadUrl = thumb_uri.toString();

                                                            mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);

                                                            HashMap<String, String> userMap = new HashMap<>();

                                                            userMap.put("name", name);
                                                            userMap.put("image", download_url);

                                                            if (!TextUtils.isEmpty(status.trim())){
                                                                userMap.put("status",status);
                                                            } else{
                                                                userMap.put("status", "Hi there I'm using Circle Messenger");
                                                            }
                                                            userMap.put("thumb_image", thumbDownloadUrl);
                                                            userMap.put("device_token", device_token);

                                                            mDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    progressDialog.dismiss();
                                                                    Intent intent = new Intent(ShowNameDialog.this, MainActivity.class);
                                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                                    startActivity(intent);
                                                                    finish();
                                                                    restart();
                                                                }
                                                            });
                                                        }
                                                    });
                                            }
                                        });
                                    }
                                });


                          /*        .addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> thumb_task) {

                                if (thumb_task.isSuccessful()) {
                                    final String download_url = thumb_task.getResult().getMetadata().getReference().getDownloadUrl().toString();
                                    UploadTask thumbUpload = thumb_Path.putBytes(imageByte);
                                    progressDialog.setProgress(0);

                                    thumbUpload.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                                            //sets and increments value of progressbar
                                            progressDialog.incrementProgressBy((int) progress);
                                        }
                                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                                                mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);

                                                HashMap<String, String> userMap = new HashMap<>();

                                                userMap.put("name", name);
                                                userMap.put("image", "default");

                                                if (!TextUtils.isEmpty(status.trim())){
                                                    userMap.put("status",status);
                                                } else{
                                                    userMap.put("status", "Hi there I'm using Circle Messenger");
                                                }
                                                userMap.put("thumb_image", "default");
                                                userMap.put("device_token", device_token);

                                                mDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        progressDialog.dismiss();
                                                        Intent intent = new Intent(ShowNameDialog.this, MainActivity.class);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        startActivity(intent);
                                                        finish();
                                                    }
                                                });

                                        }
                                    });

                                          .addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                                            String thumb_downloadUrl = task.getResult().getMetadata().getReference().getDownloadUrl().toString();
                                            if (task.isSuccessful()) {

                                                mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);

                                                HashMap<String, String> userMap = new HashMap<>();

                                                userMap.put("name", name);
                                                userMap.put("image", "default");

                                                if (!TextUtils.isEmpty(status.trim())){
                                                    userMap.put("status",status);
                                                } else{
                                                    userMap.put("status", "Hi there I'm using Circle Messenger");
                                                }
                                                userMap.put("thumb_image", "default");
                                                userMap.put("device_token", device_token);

                                                mDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        progressDialog.dismiss();
                                                        Intent intent = new Intent(ShowNameDialog.this, MainActivity.class);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        startActivity(intent);
                                                        finish();
                                                    }
                                                });
                                            }
}
                                        }
                                    });  */



                            }
                        });


                    }else {
                        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);


                        HashMap<String, String> userMap = new HashMap<String, String>();

                        userMap.put("name", name);
                        userMap.put("image", "default");
                        if (!TextUtils.isEmpty(status.trim())){
                            userMap.put("status",status);
                        } else{
                            userMap.put("status", "Hi there I'm using Circle Messenger");
                        }
                        userMap.put("thumb_image","default");
                        userMap.put("device_token",device_token);

                        mDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                                if (task.isSuccessful()) {
//                                     mRegProgress.dismiss();

                                   //  progressDialog.dismiss();
                                                Intent intent = new Intent(ShowNameDialog.this, MainActivity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(intent);
                                                finish();
                                          restart();
                                }

                            }
                        });

                    }



                }

                }


        });

        uImage.setOnClickListener(view ->
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1,1)
                    .start(ShowNameDialog.this)
        );

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_PICK && resultCode == RESULT_OK) {
            Uri imageUri = data.getData();
            CropImage.activity(imageUri).setAspectRatio(1,1).start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                image = resultUri;

                File thumb_filePath = new File(resultUri.getPath());
                //  FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();

                // String uid = current_user.getUid();

                //----------IMAGE COMPRESSION---------------

                Bitmap thumb_bitmap;
                thumb_bitmap=null;
                try {
                    thumb_bitmap = new Compressor(this)
                            .setMaxHeight(200)
                            .setMaxWidth(200)
                            .setQuality(50)
                            .compressToBitmap(thumb_filePath);

                }catch (java.io.IOException e){

                }
                ByteArrayOutputStream baos= new ByteArrayOutputStream();
                thumb_bitmap.compress(Bitmap.CompressFormat.JPEG,100,baos);
                final byte[] thumb_byte=baos.toByteArray();

                imageByte = thumb_byte;

             /*   Bitmap compressedImage = null;

                try {

                    compressedImage = new Compressor(this)
                            .setQuality(50)
                            .setMaxHeight(200)
                            .setMaxWidth(200)
                            .compressToBitmap(thumb_filePath);
                    Log.d("ImageCompression", "Success");

                } catch (java.io.IOException e) {
                    Log.d("ImageCompression", "Failure");
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                compressedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                final byte[] image_byte = baos.toByteArray();
                imageByte = image_byte; */
                Bitmap bmp = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);

        //        uImage.setImageBitmap(Bitmap.createScaledBitmap(bmp, uImage.getWidth(), uImage.getHeight(), false));
                uImage.setImageBitmap(bmp);

                Picasso.with(ShowNameDialog.this).load(image).
                        transform(new BlurTransformation(getApplicationContext(),50))
                        .into(profileBg);
                //----------IMAGE COMPRESSION---------------


            }
        }
    }


    public void restart(){

        AlarmManager alm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alm.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, PendingIntent.getActivity(this, 0, new Intent(this, com.sdskapps.circle.MainActivity.class), 0));

        Process.killProcess(Process.myPid());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mDatabase.removeEventListener(mListner);
    }

}
