package com.sdskapps.circle;


import android.content.Context;
import android.view.View;
import android.widget.PopupMenu;


/**
 * Created by shubhkotnala on 17/3/18.
 */

public class MapLinkPopupMenu extends PopupMenu {

    public MapLinkPopupMenu(Context context, View anchor) {
        super(context, anchor);
        getMenuInflater().inflate(R.menu.map_link_menu, getMenu());
    }
}