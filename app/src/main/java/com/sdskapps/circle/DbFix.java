package com.sdskapps.circle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jaredrummler.cyanea.app.CyaneaAppCompatActivity;

public class DbFix extends CyaneaAppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_db_fix);

        String user_id = getIntent().getStringExtra("user_id");
        String currentUserId = FirebaseAuth.getInstance().getUid();

        TextView textView = (TextView) findViewById(R.id.txtDbFix);

        DatabaseReference chats = FirebaseDatabase.getInstance().getReference().child("messages");
        DatabaseReference newChats = FirebaseDatabase.getInstance().getReference().child("Messages");

//        chats.child(user_id).child(currentUserId).addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//                String timeStamp = dataSnapshot.child("time").getValue().toString();
//                newChats.child(user_id).child(currentUserId).child(timeStamp).setValue(dataSnapshot.getValue());
//                Log.d("DataSnapshot",dataSnapshot.getValue().toString());
//            }
//
//            @Override
//            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//            }
//
//            @Override
//            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

        chats.child(currentUserId).child(user_id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                long size = dataSnapshot.getChildrenCount();
                long divisor = size / 100;

                ProgressDialog progressDialog = new ProgressDialog(DbFix.this);
                progressDialog.setMax(100);
                progressDialog.setTitle("Cloning");
                progressDialog.setMessage("Cloning data...");
                progressDialog.setProgress(0);
                progressDialog.setIndeterminate(false);
//
                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
              //  progressDialog.show();

                long i=0;
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    progressDialog.setProgress((int)(++i/size)*100);
                    textView.setText(""+i);

                    if(i==size){
                     //   textView.setText("DONE!");
                    }

                }

               // textView.setText("DONE!");


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

//        chats.child(currentUserId).child(user_id).addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//                String timeStamp = dataSnapshot.child("time").getValue().toString();
//                newChats.child(currentUserId).child(user_id).child(timeStamp).setValue(dataSnapshot.getValue())
//                .addOnSuccessListener(new OnSuccessListener<Void>() {
//                    @Override
//                    public void onSuccess(Void aVoid) {
//                        ProgressBar progressBar = new ProgressBar(getApplicationContext(),null,android.R.attr.progressBarStyle);
//
//                    }
//                });
//                Log.d("DataSnapshot",dataSnapshot.getValue().toString());
//            }
//
//            @Override
//            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//            }
//
//            @Override
//            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

    }
}
