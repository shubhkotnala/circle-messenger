package com.sdskapps.circle;


import android.app.Activity;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import androidx.recyclerview.widget.RecyclerView;
import android.text.format.DateUtils;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.peekandpop.shalskar.peekandpop.PeekAndPop;
import com.sdskapps.circle.CircleRectTransition.CircleRectView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.polaric.colorful.Colorful;

import java.io.File;
import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityOptionsCompat;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;
import me.saket.bettermovementmethod.BetterLinkMovementMethod;

import static android.content.Context.DOWNLOAD_SERVICE;

/**
 * Created by Shubhankar on 8/2/2017.
 */

public class MessageAdapter extends RecyclerView.Adapter{
    final Transformation transformation = new RoundedCornersTransformation(15,1);

    public static final int MESSAGE_SENT = 1;
    public static final int MESSAGE_RECEIVED = 2;
    public static  final int MESSAGE_DOC_SENT = 11;
    public static final int MESSAGE_DOC_RECEIVED = 21;

    private int color= Colorful.getThemeDelegate().getPrimaryColor().getColorRes();
    private List<Messages> mMessageList;
    private DatabaseReference mUserDatabase;
    Context ctx;
    ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
    // generate random color
    int color1 = generator.getRandomColor();

    public String imageSeen = null;

    public MessageAdapter(List<Messages> mMessageList, Context ctx) {

        this.mMessageList = mMessageList;
        this.ctx = ctx;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view;

        if(viewType == MESSAGE_SENT){
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_sent,parent,false);
            return new SentMessageHolder(view,ctx);
        } else if(viewType == MESSAGE_RECEIVED){
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_received,parent,false);
            return new ReceivedMessageHolder(view,ctx);
           }
           else if(viewType == MESSAGE_DOC_SENT){
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_document_sent,parent,false);
            return new SentDocHolder(view,ctx);

        }else if(viewType == MESSAGE_DOC_RECEIVED){
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_document_received,parent,false);
            return new ReceivedDocHolder(view,ctx);
        }

        return null;

     /*   View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_single_layout ,parent, false);

        return new MessageViewHolder(v); */

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Messages c = mMessageList.get(position);

        switch (holder.getItemViewType()){

            case MESSAGE_SENT:
                ((SentMessageHolder)holder).bind(c,imageSeen);
                break;
            case MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(c);
                break;
            case MESSAGE_DOC_SENT:
                ((SentDocHolder)holder).bind(c,imageSeen);
                break;
            case MESSAGE_DOC_RECEIVED:
                ((ReceivedDocHolder)holder).bind(c);
                break;
        }

    }
    @Override
    public int getItemViewType(int position) {

        Messages c = mMessageList.get(position);
        String from_user = c.getFrom();
        String current_user_id = FirebaseAuth.getInstance().getCurrentUser().getUid();

        if (from_user.equals(current_user_id)) {
            // If the current user is the sender of the message
          //  return MESSAGE_SENT;
            if(c.getType().startsWith("document")){
                return MESSAGE_DOC_SENT;
            }else{
              return MESSAGE_SENT;
            }
        } else {

            mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(from_user);

            mUserDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    imageSeen = dataSnapshot.child("thumb_image").getValue().toString();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            // If some other user sent the message
           // return MESSAGE_RECEIVED;
            if(c.getType().startsWith("document")){
                return MESSAGE_DOC_RECEIVED;
            }else{
                return MESSAGE_RECEIVED;
            }
        }
    }

    /*
        public class MessageViewHolder extends RecyclerView.ViewHolder {

            public TextView messageText;
            public CircleImageView profileImage;
            public TextView displayName;
            public ImageView messageImage;
            public TextView messageTime;
            public RelativeLayout relativeLayout;
            //--------------REPEAT-ALL---------------
            public TextView sentmessageText;
            public CircleImageView sentprofileImage;
            public TextView sentdisplayName;
            public ImageView sentmessageImage;
            public TextView sentmessageTime;
            public RelativeLayout sentrelativeLayout;

            //----------Relative layout for color---------
            private RelativeLayout srl;

            public MessageViewHolder(View view) {
                super(view);

                messageText = (TextView) view.findViewById(R.id.message_text_layout);
                profileImage = (CircleImageView) view.findViewById(R.id.message_profile_layout);
                displayName = (TextView) view.findViewById(R.id.name_text_layout);
                messageImage = (ImageView) view.findViewById(R.id.message_image_layout);
                messageTime = (TextView) view.findViewById(R.id.time_text_layout);
                relativeLayout = (RelativeLayout) view.findViewById(R.id.received_layout);

                //------------AGAIN FOR SENT MESSAGES-----------
                sentmessageText = (TextView) view.findViewById(R.id.sent_message_text_layout);
                sentprofileImage = (CircleImageView) view.findViewById(R.id.sent_message_profile_layout);
                sentdisplayName = (TextView) view.findViewById(R.id.sent_name_text_layout);
                sentmessageImage = (ImageView) view.findViewById(R.id.sent_message_image_layout);
                sentmessageTime = (TextView) view.findViewById(R.id.sent_time_text_layout);
                sentrelativeLayout = (RelativeLayout) view.findViewById(R.id.sent_layout);

                srl = (RelativeLayout) view.findViewById(R.id.message_layout);

             //   BetterLinkMovementMethod.linkify(Linkify.ALL,messageText,sentmessageText);



            }
        }




        @Override
        public void onBindViewHolder(final MessageViewHolder viewHolder, int i) {

            String current_user_id = FirebaseAuth.getInstance().getCurrentUser().getUid();
            Messages c = mMessageList.get(i);
            GradientDrawable bgShape = (GradientDrawable)viewHolder.srl.getBackground();
           // GradientDrawable bgImage = (GradientDrawable) viewHolder.sentmessageImage.getBackground();
           // bgImage.setStroke(7,Color.GRAY);
          //  GradientDrawable bgImageReceived = (GradientDrawable) viewHolder.messageImage.getBackground();
            //bgImageReceived.setStroke(7,viewHolder.messageImage.getContext().getResources().getColor(color));

            String from_user = c.getFrom();
            String message_type = c.getType();

            final String fUser = from_user;
            final String cUser = current_user_id;

            long timeStamp = c.getTime();
            String time = DateUtils.formatDateTime(viewHolder.messageTime.getContext(), timeStamp, DateUtils.FORMAT_SHOW_TIME);

            mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(from_user);
            bgShape.setColor(viewHolder.srl.getContext().getResources().getColor(color));
            mUserDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    String name = dataSnapshot.child("name").getValue().toString();
                    String image = dataSnapshot.child("thumb_image").getValue().toString();

                  //  String time = DateUtils.formatDateTime(, 1378798459, DateUtils.FORMAT_SHOW_TIME);
                    viewHolder.displayName.setText(name+ " ");
                    viewHolder.sentdisplayName.setText(name+" ");

                    if(!fUser.equals(cUser)){

                        String key = dataSnapshot.getKey();


                    }

                    TextDrawable drawable = TextDrawable.builder() .beginConfig()
                            .width(60)
                            .height(60)
                            .endConfig()
                            .buildRound(Character.toString(name.charAt(0)).toUpperCase(), color1);




                    Picasso.with(viewHolder.profileImage.getContext()).load(image)
                            .placeholder(drawable).into(viewHolder.profileImage);

                    Picasso.with(viewHolder.sentprofileImage.getContext()).load(image)
                            .placeholder(drawable).into(viewHolder.sentprofileImage);


                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });





            if(from_user.equals(current_user_id)){
                viewHolder.relativeLayout.setVisibility(View.GONE);
                viewHolder.sentrelativeLayout.setVisibility(View.VISIBLE);
                if(message_type.equals("text")){
                   final String msg = c.getMessage().toString();
                    if(msg.startsWith("load.sad.gif")){
                        viewHolder.sentmessageText.setVisibility(View.GONE);
                        viewHolder.sentmessageImage.setVisibility(View.VISIBLE);
                        Glide.with(ctx).load(Uri.parse("file:///android_asset/"+msg)).asGif().override(40,40).fitCenter().crossFade().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(viewHolder.sentmessageImage);

                    } else{
                        viewHolder.sentmessageText.setVisibility(View.VISIBLE);
                        viewHolder.sentmessageText.setText(c.getMessage()+" ");
                        Linkify.addLinks(viewHolder.sentmessageText,Linkify.ALL);
                        // BetterLinkMovementMethod.linkify(Linkify.ALL,viewHolder.sentmessageText).setOnLinkClickListener(urlClickListener);
                        viewHolder.sentmessageImage.setVisibility(View.GONE);
                    }
                }  else {

                    viewHolder.sentmessageText.setVisibility(View.GONE);
                    viewHolder.sentmessageImage.setVisibility(View.VISIBLE);
                    //ViewCompat.setTransitionName(viewHolder.sentmessageImage,"shredMessageImage");

                    Picasso.with(viewHolder.sentprofileImage.getContext()).load(c.getMessage())
                            .transform(transformation)
                            .fit().centerCrop().placeholder(R.drawable.default_avatar1).into(viewHolder.sentmessageImage);
                    final String mi = c.getMessage();
                    viewHolder.sentmessageImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent messageImageIntent = new Intent(viewHolder.sentmessageImage.getContext(),MessageImageViewer.class);
                            messageImageIntent.putExtra("messageImage",mi);
                           ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity)ctx,viewHolder.sentmessageImage,"msgImage");
                            viewHolder.sentmessageImage.getContext().startActivity(messageImageIntent,options.toBundle());
                        }
                    });

                }
                viewHolder.sentmessageTime.setText(time);
              //  bgShape.setColor(Color.GRAY);
    //            bgShape.setColor(viewHolder.sentrelativeLayout.getContext().getResources().getColor(color));

            } else {

               // bgShape.setColor(viewHolder.srl.getContext().getResources().getColor(color));
                viewHolder.sentrelativeLayout.setVisibility(View.GONE);
                viewHolder.relativeLayout.setVisibility(View.VISIBLE);
                if(message_type.equals("text")){

                    final String msg = c.getMessage().toString();
                    if(msg.startsWith("load.sad.gif")){
                        viewHolder.messageImage.setVisibility(View.VISIBLE);
                        Glide.with(ctx).load(Uri.parse("file:///android_asset/"+msg)).asGif().override(40,40).crossFade().fitCenter().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(viewHolder.messageImage);
                        viewHolder.messageText.setVisibility(View.GONE);
                    } else{
                        viewHolder.messageText.setVisibility(View.VISIBLE);
                        viewHolder.messageText.setText(c.getMessage()+" ");
                        viewHolder.messageImage.setVisibility(View.GONE);
                        Linkify.addLinks(viewHolder.messageText,Linkify.ALL);
                    }
                   // BetterLinkMovementMethod.linkify(Linkify.ALL,viewHolder.messageText).setOnLinkClickListener(urlClickListener);


                }  else {

                    viewHolder.messageText.setVisibility(View.GONE);
                    viewHolder.messageImage.setVisibility(View.VISIBLE);
                  //  ViewCompat.setTransitionName(viewHolder.sentmessageImage,"shredMessageImage");
                    Picasso.with(viewHolder.profileImage.getContext()).load(c.getMessage())
                            .transform(transformation)
                            .fit().centerCrop().placeholder(R.drawable.default_avatar1).into(viewHolder.messageImage);
                    final String mi = c.getMessage();
                    viewHolder.messageImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent messageImageIntent = new Intent(viewHolder.messageImage.getContext(),MessageImageViewer.class);
                            messageImageIntent.putExtra("messageImage",mi);
                            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity)ctx,viewHolder.sentmessageImage,"msgImage");

                            viewHolder.messageImage.getContext().startActivity(messageImageIntent,options.toBundle());
                        }
                    });


                }
             //   bgShape.setColor(Color.GRAY);

            }
            viewHolder.messageTime.setText(time);

           // viewHolder.messageText.setText(c.getMessage());

        }

    */
    @Override
    public int getItemCount() {
        return mMessageList.size();
    }


    //---------EXTRA FOR MESSAGE LAYOUTS-------------

    private final BetterLinkMovementMethod.OnLinkClickListener urlClickListener = new BetterLinkMovementMethod.OnLinkClickListener() {
        @Override
        public boolean onClick(TextView view, String url) {
            if (isPhoneNumber(url)) {
                PhoneLinkPopupMenu phonePopupMenu = new PhoneLinkPopupMenu(ctx, view, url);
                phonePopupMenu.show();

            } else if (isEmailAddress(url)) {
                EmailLinkPopupMenu emailPopupMenu = new EmailLinkPopupMenu(ctx, view);
                emailPopupMenu.show();

            } else if (isMapAddress(url)) {
                MapLinkPopupMenu mapPopupMenu = new MapLinkPopupMenu(ctx, view);
                mapPopupMenu.show();

            }

            return true;
        }
    };


    private boolean isPhoneNumber(String url) {
        return url.startsWith("tel:");
    }

    private boolean isEmailAddress(String url) {
        return url.contains("@");
    }

    private boolean isMapAddress(String url) {
        return url.contains("goo.gl/maps");
    }

    /**
     * Created by shubhkotnala on 30/3/18.
     */

    public static class ReceivedMessageHolder extends RecyclerView.ViewHolder {

        public TextView messageText;
        public CircleImageView profileImage;
        public TextView displayName;
        public CircleRectView messageImage;
        public TextView messageTime;
        private Context ctx;

        final Transformation transformation = new RoundedCornersTransformation(10,1);

        ReceivedMessageHolder(View itemView, Context ctx) {
            super(itemView);
            this.ctx = ctx;
            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
          //  profileImage = (CircleImageView) itemView.findViewById(R.id.image_message_profile);
            messageTime = (TextView) itemView.findViewById(R.id.text_message_time);
            messageImage = (CircleRectView) itemView.findViewById(R.id.text_message_image);
        }

        void bind(final Messages messages){

           // messageText.setText(messages.getMessage());
            long timeStamp = messages.getTime();
            String time = DateUtils.formatDateTime(itemView.getContext(), timeStamp, DateUtils.FORMAT_SHOW_TIME);
            messageTime.setText(time);

            Linkify.addLinks(messageText,Linkify.ALL);

            if(messages.getType().equals("image")){
                messageText.setVisibility(View.GONE);
                messageImage.setVisibility(View.VISIBLE);
                final String mi = messages.getMessage();
                Picasso.with(messageImage.getContext()).load(messages.getMessage())
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .transform(transformation)
                        .fit().centerCrop().placeholder(R.drawable.default_avatar1).into(messageImage, new Callback() {
                    @Override
                    public void onSuccess() {


                        PeekAndPop peekAndPop = new PeekAndPop.Builder((Activity)ctx)
                                .peekLayout(R.layout.activity_message_image_viewer)
                                .longClickViews(messageImage)
                                .build();

                        View peekView = peekAndPop.getPeekView();
                        CircleRectView circleRectView = peekView.findViewById(R.id.settings_image);
                        Picasso.with(ctx).load(mi).networkPolicy(NetworkPolicy.OFFLINE).into(circleRectView);

                    }

                    @Override
                    public void onError() {
                        Picasso.with(messageImage.getContext()).load(messages.getMessage())
                                .transform(transformation)
                                .fit().centerCrop().placeholder(R.drawable.default_avatar1).into(messageImage, new Callback() {
                            @Override
                            public void onSuccess() {

                                PeekAndPop peekAndPop = new PeekAndPop.Builder((Activity)ctx)
                                        .peekLayout(R.layout.activity_message_image_viewer)
                                        .longClickViews(messageImage)
                                        .build();

                                View peekView = peekAndPop.getPeekView();
                                TouchImageView circleRectView = peekView.findViewById(R.id.settings_image);
                                Picasso.with(ctx).load(mi).networkPolicy(NetworkPolicy.OFFLINE).into(circleRectView);

                            }

                            @Override
                            public void onError() {

                            }
                        });
                    }
                });


                messageImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent messageImageIntent = new Intent(messageImage.getContext(),MessageImageViewer.class);
                        messageImageIntent.putExtra("messageImage",mi);
                        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity)ctx,messageImage,"msgImage");
                        messageImage.getContext().startActivity(messageImageIntent,options.toBundle());

                    }
                });

            } else if(messages.getType().equals("text")) {
                String msg = messages.getMessage().toString();
                if(msg.startsWith("load.sad.gif")){

                    Glide.with(messageImage.getContext()).load(Uri.parse("file:///android_asset/"+msg)).asGif()
                           // .override(120,120).fitCenter().crossFade()
                            .dontTransform()
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE).into(messageImage);
                    messageText.setVisibility(View.GONE);
                    messageImage.setVisibility(View.VISIBLE);
                 //   messageText.setText(messages.getMessage());

                } else{

                    messageText.setVisibility(View.VISIBLE);
                    messageImage.setVisibility(View.GONE);
                    messageText.setText(messages.getMessage());
                    Linkify.addLinks(messageText,Linkify.ALL);
                }
            }
         //   Picasso.with(messageImage.getContext()).load()

        }

    }

    /**
     * Created by shubhkotnala on 30/3/18.
     */

    public static class SentMessageHolder extends RecyclerView.ViewHolder{

        public TextView messageText;
        public CircleImageView profileImage;
        public TextView displayName;
        public CircleRectView messageImage;
        public TextView messageTime;
        private Context ctx;
        private CircleImageView txtSeen;
        private Messages msg;

        final Transformation transformation = new RoundedCornersTransformation(10,1);

        public SentMessageHolder(View itemView,Context ctx) {
            super(itemView);
            this.ctx = ctx;
            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            messageTime = (TextView) itemView.findViewById(R.id.text_message_time);
            messageImage = (CircleRectView) itemView.findViewById(R.id.text_message_image);
            txtSeen = (CircleImageView) itemView.findViewById(R.id.txtSeen);
            itemView.setOnLongClickListener(v -> {
                final CharSequence[] items = {"Delete", "Share External", "Edit"};

                AlertDialog.Builder builder = new AlertDialog.Builder(ctx);

                builder.setTitle("Select The Action");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {

                       // Toast.makeText(ctx, "item : " + item, Toast.LENGTH_SHORT).show();

                        if(item == 0){
                          //  Toast.makeText(ctx, msg.getKey(), Toast.LENGTH_SHORT).show();
                            SharedPreferences chatUser = ctx.getSharedPreferences("chatDetails", 0);
                            String chatUserId = chatUser.getString("chatUserId", "default");
                            String currentUseRId = chatUser.getString("currentUserId","default");
                            FirebaseDatabase.getInstance().getReference().child("messages").child(chatUserId).child(currentUseRId).child(msg.getKey()).removeValue();
                            FirebaseDatabase.getInstance().getReference().child("messages").child(currentUseRId).child(chatUserId).child(msg.getKey()).removeValue();

                        } else{
                            Toast.makeText(ctx, "Comming Soon!", Toast.LENGTH_SHORT).show();
                        }
                        /*else if(item == 1){
                            Intent sharingIntent = new Intent("android.intent.action.SEND");
                            sharingIntent.setType("text/plain");
                            String sharingBody = messageText.getText().toString() ;
                            sharingIntent.putExtra("android.intent.extra.SUBJECT", "Quote of the Day");
                            sharingIntent.putExtra("android.intent.extra.TEXT", sharingBody);
                            startActivity(Intent.createChooser(sharingIntent, "Share via..."));
                        } else if(item == 2){
                            //TODO copy....but nahi kuch aur krte hai
                        }  */

                        }
                });
                builder.show();
                return true;
            });
        }
        void bind(Messages messages,String imageSeen){
         //   messageText.setText(messages.getMessage());
            long timeStamp = messages.getTime();
            msg=messages;
            String time = DateUtils.formatDateTime(itemView.getContext(), timeStamp, DateUtils.FORMAT_SHOW_TIME);
            messageTime.setText(time);
            Linkify.addLinks(messageText,Linkify.ALL);

            Log.d("Sent Message Seen",""+messages.isSeen());

            if (messages.isSeen()){
                txtSeen.setVisibility(View.VISIBLE);

                    Picasso.with(ctx).load(imageSeen).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.default_avatar1).into(txtSeen, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Picasso.with(ctx).load(imageSeen).placeholder(R.drawable.default_avatar1).into(txtSeen);
                        }
                    });


            } else {
                Picasso.with(ctx).load(R.drawable.circlexml).into(txtSeen);
            }

            if(messages.getType().equals("image")){
                messageText.setVisibility(View.GONE);

                final String mi = messages.getMessage();
                messageImage.setVisibility(View.VISIBLE);

                Picasso.with(messageImage.getContext()).load(messages.getMessage())
                       // .transform(transformation)
                        .networkPolicy(NetworkPolicy.OFFLINE)
                       // .fit().centerCrop()
                        .placeholder(R.drawable.default_avatar1).into(messageImage, new Callback() {
                    @Override
                    public void onSuccess() {

                        PeekAndPop peekAndPop = new PeekAndPop.Builder((Activity)ctx)
                                .peekLayout(R.layout.activity_message_image_viewer)
                                .longClickViews(messageImage)
                                .build();

                        View peekView = peekAndPop.getPeekView();
                        CircleRectView circleRectView = peekView.findViewById(R.id.settings_image);
                        Picasso.with(ctx).load(mi).networkPolicy(NetworkPolicy.OFFLINE).into(circleRectView);

                    }

                    @Override
                    public void onError() {
                        Picasso.with(messageImage.getContext()).load(mi)
                              //  .transform(transformation)
                                //                                                  .fit().centerCrop()
                                .placeholder(R.drawable.default_avatar1).into(messageImage, new Callback() {
                            @Override
                            public void onSuccess() {

                                PeekAndPop peekAndPop = new PeekAndPop.Builder((Activity)ctx)
                                        .peekLayout(R.layout.activity_message_image_viewer)
                                        .longClickViews(messageImage)
                                        .build();

                                View peekView = peekAndPop.getPeekView();
                                TouchImageView circleRectView = peekView.findViewById(R.id.settings_image);
                                Picasso.with(ctx).load(mi).networkPolicy(NetworkPolicy.OFFLINE).into(circleRectView);

                            }

                            @Override
                            public void onError() {

                            }
                        });
                    }
                });

               /* messageImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent messageImageIntent = new Intent(messageImage.getContext(),MessageImageViewer.class);
                        messageImageIntent.putExtra("messageImage",mi);
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation((Activity)ctx,messageImage,"msgImage");
                        messageImage.getContext().startActivity(messageImageIntent,options.toBundle());
                    }
                });  */

               messageImage.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                       Intent messageImageIntent = new Intent(messageImage.getContext(),MessageImageViewer.class);
                       messageImageIntent.putExtra("messageImage",mi);
                       ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity)ctx,messageImage,"msgImage");
                       messageImage.getContext().startActivity(messageImageIntent,options.toBundle());
                   }
               });

            } else if(messages.getType().equals("text")) {

                String msg = messages.getMessage().toString();

                if(msg.startsWith("load.sad.gif")){

                    Glide.with(ctx).load(Uri.parse("file:///android_asset/"+msg)).asGif()
                         //   .override(120,120).fitCenter().crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                           .dontTransform()
                    .into(messageImage);
                    messageText.setVisibility(View.GONE);
                    messageImage.setVisibility(View.VISIBLE);
                  //  messageText.setText(messages.getMessage());
                    //Ion.with(messageImage)
                    //        .load("file:///android_asset/"+msg);

                } else{

                    messageText.setVisibility(View.VISIBLE);
                    messageImage.setVisibility(View.GONE);
                    messageText.setText(messages.getMessage());
                    Linkify.addLinks(messageText,Linkify.ALL);

                }

            }

        }
    }

    public static class ReceivedDocHolder extends RecyclerView.ViewHolder {

        public TextView messageText;
        private Context ctx;
        private TextView attachLabel;
        private TextView attachName;
        private TextView attachSize;

        final Transformation transformation = new RoundedCornersTransformation(10,1);

        ReceivedDocHolder(View itemView, Context ctx) {
            super(itemView);
            this.ctx = ctx;
            //  messageTime = (TextView) itemView.findViewById(R.id.text_message_time);
            attachLabel = (TextView) itemView.findViewById(R.id.attachReceivedLabel);
            attachName = (TextView) itemView.findViewById(R.id.attachReceivedName);
            attachSize = (TextView) itemView.findViewById(R.id.attachReceivedSize);
        }

        void bind(final Messages messages){

            // messageText.setText(messages.getMessage());
            long timeStamp = messages.getTime();
            String time = DateUtils.formatDateTime(itemView.getContext(), timeStamp, DateUtils.FORMAT_SHOW_TIME);
            //messageTime.setText(time);

          //  Linkify.addLinks(messageText,Linkify.ALL);

            String messageType = messages.getType();
            String fileName = messageType.substring(messageType.indexOf("~") + 1);
            String name = fileName.substring(0,fileName.lastIndexOf('.'));
            String ext = messageType.substring(messageType.lastIndexOf('.') + 1).toUpperCase();

            attachLabel.setText(ext);
            attachName.setText(fileName);
            attachSize.setText(time);
            itemView.setOnClickListener( v -> {
                String path = Environment.getExternalStorageDirectory() + "/" + "Circle" + "/" + "Circle Documents" + "/" + fileName;
                File file = new File(path);
                if(file.isFile()){
                 //   Toast.makeText(ctx, file.getName(), Toast.LENGTH_SHORT).show();
                    Toast.makeText(ctx, "Opening file...", Toast.LENGTH_SHORT).show();
                    MimeTypeMap myMime = MimeTypeMap.getSingleton();
                    Intent newIntent = new Intent(Intent.ACTION_VIEW);
                    String mimeType = myMime.getMimeTypeFromExtension(ext);
                    newIntent.setDataAndType(Uri.fromFile(file),mimeType);
                    newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    try {
                        ctx.startActivity(newIntent);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(ctx, "No handler for this type of file.", Toast.LENGTH_LONG).show();
                    }
                }
                else{
                    Toast.makeText(ctx, "Downloading...", Toast.LENGTH_SHORT).show();
                    String dirPath = Environment.getExternalStorageDirectory() + "/" + "Circle" + "/" + "Circle Documents" + "/";
                    File dir = new File(dirPath);
                    if(!dir.exists()){
                        dir.mkdirs();
                    }

                    DownloadManager downloadmanager = (DownloadManager) ctx.
                            getSystemService(DOWNLOAD_SERVICE);
                    Uri uri = Uri.parse(messages.getMessage());
                    DownloadManager.Request request = new DownloadManager.Request(uri);

                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    //  request.setDestinationInExternalFilesDir(context, destinationDirectory, fileName + fileExtension);
                    request.setDestinationInExternalPublicDir("/Circle/Circle Documents",name + "." + ext);

                    downloadmanager.enqueue(request);

                }
            });
        }

    }

    public static class SentDocHolder extends RecyclerView.ViewHolder {

        public TextView messageText;
        private TextView attachLabel;
        private TextView attachName;
        private TextView attachSize;
        private CircleImageView seen;
        private Context ctx;

        final Transformation transformation = new RoundedCornersTransformation(10,1);

        SentDocHolder(View itemView, Context ctx) {
            super(itemView);
            this.ctx = ctx;
           // messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            attachLabel = (TextView) itemView.findViewById(R.id.attachSentLabel);
            attachName = (TextView) itemView.findViewById(R.id.attachSendName);
            attachSize = (TextView) itemView.findViewById(R.id.attachSendSize);
            seen = (CircleImageView) itemView.findViewById(R.id.docSeen);
        }

        void bind(final Messages messages, String imageSeen){

            // messageText.setText(messages.getMessage());
            long timeStamp = messages.getTime();
            String time = DateUtils.formatDateTime(itemView.getContext(), timeStamp, DateUtils.FORMAT_SHOW_TIME);
            attachSize.setText(time);

//            Linkify.addLinks(messageText,Linkify.ALL);

            if (messages.isSeen()){
                seen.setVisibility(View.VISIBLE);

                Picasso.with(ctx).load(imageSeen).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.default_avatar1).into(seen, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        Picasso.with(ctx).load(imageSeen).placeholder(R.drawable.default_avatar1).into(seen);
                    }
                });


            } else {
                Picasso.with(ctx).load(R.drawable.circlexml).into(seen);
            }

            String messageType = messages.getType();
            String fileName = messageType.substring(messageType.indexOf("~") + 1);
            String name = fileName.substring(0,fileName.lastIndexOf('.'));
            String ext = messageType.substring(messageType.lastIndexOf('.') + 1).toUpperCase();

            attachLabel.setText(ext);
            attachName.setText(fileName);
           // Toast.makeText(ctx, fileName + "\n" + ext, Toast.LENGTH_SHORT).show();
            itemView.setOnClickListener( v -> {
                String path = Environment.getExternalStorageDirectory() + "/" + "Circle" + "/" + "Circle Documents" + "/" + fileName;
                File file = new File(path);
                if(file.isFile()){
                  //  Toast.makeText(ctx, file.getName(), Toast.LENGTH_SHORT).show();
                    Toast.makeText(ctx, "Opening file...", Toast.LENGTH_SHORT).show();
                    MimeTypeMap myMime = MimeTypeMap.getSingleton();
                    Intent newIntent = new Intent(Intent.ACTION_VIEW);
                    String mimeType = myMime.getMimeTypeFromExtension(ext.toLowerCase());
                    newIntent.setDataAndType(Uri.fromFile(file),mimeType);
                    newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    try {
                        ctx.startActivity(newIntent);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(ctx, "No handler for this type of file.", Toast.LENGTH_LONG).show();
                    }
                }
                else{

                    Toast.makeText(ctx, "Downloading...", Toast.LENGTH_SHORT).show();
                    String dirPath = Environment.getExternalStorageDirectory() + "/" + "Circle" + "/" + "Circle Documents" + "/";
                    File dir = new File(dirPath);
                    if(!dir.exists()){
                        dir.mkdirs();
                    }

                    DownloadManager downloadmanager = (DownloadManager) ctx.
                            getSystemService(DOWNLOAD_SERVICE);
                    Uri uri = Uri.parse(messages.getMessage());
                    DownloadManager.Request request = new DownloadManager.Request(uri);

                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    //  request.setDestinationInExternalFilesDir(context, destinationDirectory, fileName + fileExtension);
                    request.setDestinationInExternalPublicDir("/Circle/Circle Documents",name + "." + ext);

                    downloadmanager.enqueue(request);

                }
            });
        }

    }



    public void downloadFile(Context context, String fileName, String fileExtension,  String url) {

        String path = Environment.getExternalStorageDirectory() + "/" + "Circle" + "/" + "Circle Documents" + "/";
        File dir = new File(path);
        if(!dir.exists()){
            dir.mkdirs();
        }

        DownloadManager downloadmanager = (DownloadManager) context.
                getSystemService(DOWNLOAD_SERVICE);
        Uri uri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(uri);

        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
      //  request.setDestinationInExternalFilesDir(context, destinationDirectory, fileName + fileExtension);
        request.setDestinationInExternalFilesDir(context,"/Circle/Circle Documents",fileName + "." + fileExtension);

        downloadmanager.enqueue(request);

      //  return downloadmanager.enqueue(request);
    }




}

