package com.sdskapps.circle;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.FloatRange;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import agency.tango.materialintroscreen.MaterialIntroActivity;
import agency.tango.materialintroscreen.SlideFragmentBuilder;
import agency.tango.materialintroscreen.animations.IViewTranslation;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PermissionActivity extends MaterialIntroActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enableLastSlideAlphaExitTransition(true);

        getBackButtonTranslationWrapper()
                .setEnterTranslation(new IViewTranslation() {
                    @Override
                    public void translate(View view, @FloatRange(from = 0, to = 1.0) float percentage) {
                        view.setAlpha(percentage);
                    }
                });



        addSlide(new SlideFragmentBuilder()
                .backgroundColor(R.color.red)
                .buttonsColor(R.color.bGreen)
                .neededPermissions(new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
                .image(agency.tango.materialintroscreen.R.drawable.ic_next)
                .title("Missing Permissions!")
                .description("Looks like one or more permissions got revoked somehow.\nNo worries, though. WE'll help you back up and running.")
                .build()); //,
        //    new MessageButtonBehaviour(new View.OnClickListener() {
        //    @Override
        //      public void onClick(View v) {
        //          showMessage("Enjoy using Circle Messenger.\nDo note that this is pre-release version so you experience some bugs.");
        //    }
        //}, "Enjoy!"));
        addSlide(new SlideFragmentBuilder()
                .backgroundColor(R.color.bGreen)
                .buttonsColor(R.color.profileBg)
                .title("You're ready to Rock!")
                .description("Enjoy your new personal Messenger\nMade with " + getEmoji(0x2665) + " for you" + "\n\n\n Copyright " +
                        getEmoji(0x00A9)+" SDSK Apps")
                .build());

    }

    @Override
    public void onFinish() {
        super.onFinish();
        Toast.makeText(getApplicationContext(),"Enjoy using Circle Messenger.\nDo note that this is pre-release version so you may experience some bugs.",Toast.LENGTH_LONG).show();
        SharedPreferences settings=getSharedPreferences("firstCircleRun",0);
        SharedPreferences.Editor editor=settings.edit();
        editor.putBoolean("firstRun",true);
        editor.commit();
        startActivity(new Intent(PermissionActivity.this,MainActivity.class));
    }
    public String getEmoji(int unicode){
        return new String(Character.toChars(unicode));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
