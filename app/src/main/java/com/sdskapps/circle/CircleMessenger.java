package com.sdskapps.circle;

import android.app.Activity;
import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import com.jaredrummler.cyanea.Cyanea;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.one.EmojiOneProvider;

import org.polaric.colorful.Colorful;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Shubhankar on 7/24/2017.
 */

public class CircleMessenger extends Application implements
        Application.ActivityLifecycleCallbacks  {

    private DatabaseReference mUSerDatabase;
    private FirebaseAuth mAuth;
    public static final String TAG = CircleMessenger.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private static CircleMessenger mInstance;
    private DatabaseReference mGlobalNotificationDatabase;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;

        EmojiManager.install(new EmojiOneProvider());

        Cyanea.init(this,getResources());


        Colorful.defaults()
                .primaryColor(Colorful.ThemeColor.DEEP_ORANGE)
                .accentColor(Colorful.ThemeColor.LIGHT_GREEN)
                .translucent(false)
                .dark(false);
        Colorful.init(this);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(false);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);

        mGlobalNotificationDatabase = FirebaseDatabase.getInstance().getReference().child("globalNotification");
        mGlobalNotificationDatabase.keepSynced(true);

        SharedPreferences fontSettings=getSharedPreferences("fontPath",0);
        String firstFont=fontSettings.getString("path","default");

         if(!firstFont.equals("default")){

             CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                     .setDefaultFontPath(firstFont)
                     .setFontAttrId(R.attr.fontPath)
                     .build()
             );

         }

        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() != null) {

            String token = FirebaseInstanceId.getInstance().getToken();


            mUSerDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(mAuth.getCurrentUser().getUid());

            mUSerDatabase.keepSynced(true);

            DatabaseReference mMessageRef = FirebaseDatabase.getInstance().getReference().child("messages").child(mAuth.getCurrentUser().getUid());
            mMessageRef.keepSynced(true);
            
            mUSerDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if (dataSnapshot != null) {
                        mUSerDatabase.child("online").onDisconnect().setValue(ServerValue.TIMESTAMP);

                        String oldToken = dataSnapshot.child("device_token").getValue().toString();
                        if(!oldToken.equals(token)){
                            mUSerDatabase.child("device_token").setValue(token);
                        }
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    @Override
    public void onActivityResumed(Activity activity) {
        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }


    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    public static synchronized CircleMessenger getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

}
