package com.sdskapps.circle;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.jaredrummler.cyanea.app.CyaneaAppCompatActivity;

import org.polaric.colorful.CActivity;
import org.polaric.colorful.Colorful;

import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterActivity extends CyaneaAppCompatActivity {

    private int color= Colorful.getThemeDelegate().getPrimaryColor().getColorRes();

    private TextInputLayout mDisplayName;
    private TextInputLayout mEmail;
    private TextInputLayout mPassword;
    private Button mCreateBtn;
    private Button mPhoneAuth;

    private FirebaseAuth mAuth;
    private ProgressDialog mRegProgress;
    private Toolbar mToolbar;

    private RelativeLayout mRelativeLayout;

    private DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Toolbar
        mToolbar=(Toolbar) findViewById(R.id.register_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Register");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //firebase
        mAuth = FirebaseAuth.getInstance();

        mRelativeLayout = (RelativeLayout) findViewById(R.id.relativeRegisterLayout);
        mRelativeLayout.setBackgroundColor(getResources().getColor(color));

        mRegProgress= new ProgressDialog(this);
        mDisplayName=(TextInputLayout) findViewById(R.id.reg_display_name);
        mEmail=(TextInputLayout) findViewById(R.id.reg_email);
        mPassword=(TextInputLayout) findViewById(R.id.reg_password);
        mCreateBtn=(Button) findViewById(R.id.reg_create_btn);
        mPhoneAuth = (Button) findViewById(R.id.phone_auth_btn);

        mPhoneAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterActivity.this,PhoneAuthActivity.class));
                finish();
            }
        });
        mCreateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String display_name=mDisplayName.getEditText().getText().toString();
                String email=mEmail.getEditText().getText().toString();
                String password=mPassword.getEditText().getText().toString();

                if(!TextUtils.isEmpty(display_name)||!TextUtils.isEmpty(email)||!TextUtils.isEmpty(password)){
                    mRegProgress.setTitle("Registering User");
                    mRegProgress.setMessage("Please wait while we create your account");
                    mRegProgress.setCanceledOnTouchOutside(false);
                    mRegProgress.show();
                    register_user(display_name,email,password);

                }

            }
        });
    }

    private void register_user(final String display_name, String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful())
                {

                    FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
                String uid= current_user.getUid();

                // String current_user_id=mAuth.getCurrentUser().getUid();
                String deviceToken= FirebaseInstanceId.getInstance().getToken();
                mDatabase= FirebaseDatabase.getInstance().getReference().child("Users").child(uid);

                String device_token= FirebaseInstanceId.getInstance().getToken();

                HashMap<String,String> userMap= new HashMap<String, String>();
                userMap.put("device_token",deviceToken);
                userMap.put("name", display_name);
                userMap.put("status", "Hi There, I'm using Circle Messenger");
                userMap.put("image", "default");
                userMap.put("thumb_image", "default");
                userMap.put("device_token", device_token);

                mDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if(task.isSuccessful()){
                            mRegProgress.dismiss();
                            Intent mainIntent= new Intent(RegisterActivity.this,MainActivity.class);
                            mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(mainIntent);
                            finish();
                        }

                    }
                });


            }
                else{
                    mRegProgress.hide();
                    Toast.makeText(RegisterActivity.this,"Cannot create your account. Please try again later.",Toast.LENGTH_LONG).show();
                }
            }
        });


    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
