package com.sdskapps.circle;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jaredrummler.cyanea.app.CyaneaAppCompatActivity;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.polaric.colorful.CActivity;
import org.polaric.colorful.Colorful;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SharedActivity extends CyaneaAppCompatActivity {

    private Toolbar mToolbar;
    private int color = Colorful.getThemeDelegate().getPrimaryColor().getColorRes();
    private int accent = Colorful.getThemeDelegate().getAccentColor().getColorRes();
    private RelativeLayout mRelative;
    private DatabaseReference mFriendsDatabase, mUsersDatabase;
    private RecyclerView mFriendsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared);

        mToolbar = (Toolbar) findViewById(R.id.shared_page_toolbar);
        mToolbar.setElevation(0);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle("Select Contact");

        mFriendsList = (RecyclerView) findViewById(R.id.shared_friends_list);
        mFriendsList.hasFixedSize();
        mFriendsList.setLayoutManager(new LinearLayoutManager(this));

        final String mCurrentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid().toString();
        mFriendsDatabase = FirebaseDatabase.getInstance().getReference().child("Friends").child(mCurrentUserId);
        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");

        mRelative = (RelativeLayout) findViewById(R.id.shared_layout);

        int[] colors = {color, accent};
        GradientDrawable gd = new GradientDrawable();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setCornerRadius(0f);
        gd.setColors(new int[]{
                getResources().getColor(color),
                getResources().getColor(accent)
        });

        mRelative.setBackground(gd);

        final Intent chatIntent = new Intent(SharedActivity.this,ChatActivity.class);

        final Intent intent = getIntent();
        final String action = intent.getAction();
        final String type = intent.getType();

        final FirebaseRecyclerAdapter<Friends, FriendsFragment.FriendsViewHolder> friendsRecyclerViewAdapter= new FirebaseRecyclerAdapter<Friends, FriendsFragment.FriendsViewHolder>(
                Friends.class,
                R.layout.users_single_layout,
                FriendsFragment.FriendsViewHolder.class,
                mFriendsDatabase
        ) {
            @Override
            protected void populateViewHolder(final FriendsFragment.FriendsViewHolder viewHolder, Friends friends, int i) {

                viewHolder.setDate(friends.getDate());
                final String list_user_id=getRef(i).getKey();
                mUsersDatabase.child(list_user_id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot dataSnapshot) {

                        final String userName= dataSnapshot.child("name").getValue().toString();
                        final String userThumb= dataSnapshot.child("thumb_image").getValue().toString();
                       final String imageUri = dataSnapshot.child("image").getValue().toString();
                       final String status = dataSnapshot.child("status").getValue().toString();
                        if(dataSnapshot.hasChild("online")) {
                            String userOnline =dataSnapshot.child("online").getValue().toString();
                            viewHolder.setUserOnline(userOnline);
                        }

                        viewHolder.setName(userName);
                        viewHolder.setUserImage(userName,userThumb,getApplicationContext());
                        viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                SharedPreferences sharedSetting = getSharedPreferences("sharedContent",0);
                                SharedPreferences.Editor editor = sharedSetting.edit();

                                chatIntent.putExtra("user_id",list_user_id);
                                chatIntent.putExtra("user_name",userName);
                                chatIntent.putExtra("status",status);

                                if (Intent.ACTION_SEND.equals(action) && type != null) {
                                    if ("text/plain".equals(type)) {
                                         chatIntent.putExtra("sharedText",intent.getStringExtra(Intent.EXTRA_TEXT));
                                       startActivity(chatIntent);
                                        String text = intent.getStringExtra(Intent.EXTRA_TEXT);
                                            editor.putString("sharedText",text);
                                            Toast.makeText(SharedActivity.this,text,Toast.LENGTH_SHORT).show();
                                        //handleSendText(intent);
                                        // Handle text being sent

                                    } else if (type.startsWith("image/")) {
                                         chatIntent.putExtra("sharedimage",intent.getParcelableExtra(Intent.EXTRA_STREAM).toString());
                                        startActivity(chatIntent);
                                        String image = intent.getParcelableExtra(Intent.EXTRA_STREAM).toString();
                                        editor.putString("sharedImage",image);
                                        Toast.makeText(SharedActivity.this,image,Toast.LENGTH_SHORT).show();
                                        //handleSendImage(intent);
                                        // Handle single image being sent
                                    }else if (!intent.getStringExtra("sharedText").trim().isEmpty() && intent.getStringExtra("sharedText") != null){
                                        editor.putString("sharedText",intent.getStringExtra("sharedText").trim().toString());
                                        chatIntent.putExtra("sharedText",intent.getStringExtra("sharedText").trim());
                                        startActivity(chatIntent);
                                    }
                                }

                            }
                        });

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        };
        mFriendsList.setAdapter(friendsRecyclerViewAdapter);



        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
               // chatIntent.putExtra("sharedText",intent.getStringExtra(Intent.EXTRA_TEXT));
                handleSendText(intent);
                // Handle text being sent
            } else if (type.startsWith("image/")) {
              //  chatIntent.putExtra("sharedimage",intent.getParcelableExtra(Intent.EXTRA_STREAM));
                handleSendImage(intent);
                // Handle single image being sent
            }
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
            if (type.startsWith("image/")) {
                handleSendMultipleImages(intent); // Handle multiple images being sent
            }
        } else if(!intent.getStringExtra("sharedText").trim().isEmpty() && intent.getStringExtra("sharedText") != null) {
           // handleSendText(intent);
            // Handle other intents, such as being started from the home screen
        }

    }

    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            // TODO : handle text messages being sent
         //   Toast.makeText(SharedActivity.this,"Text received to share.",Toast.LENGTH_SHORT).show();
            Snackbar.make(mRelative,"Text Received!",Snackbar.LENGTH_SHORT).show();
        }
    }

    void handleSendImage(Intent intent) {
        Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            // TODO : handle uri for single image
            Snackbar.make(mRelative,"Image Received!" + imageUri,Snackbar.LENGTH_SHORT).show();
        }
    }

    void handleSendMultipleImages(Intent intent) {
        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (imageUris != null) {
            // TODO : handle multiple uri for images
            Snackbar.make(mRelative,"Multiple Images Received! \n Cannot handle multiple images.",Snackbar.LENGTH_SHORT).show();
        }
    }

    public static class FriendsViewHolder extends RecyclerView.ViewHolder {

        View mView;
        CircleImageView userImageView;
        public FriendsViewHolder(View itemView){
            super(itemView);
            mView=itemView;
        }
        public void setDate(String date){
            TextView userStatusView= (TextView) mView.findViewById(R.id.user_single_status);
            userStatusView.setText(date+" ");
        }
        public void setName(String name){
            TextView userNameView = (TextView) mView.findViewById(R.id.user_single_name);
            userNameView.setText(name+" ");
        }
        public void setUserImage(String uName,String thumb_image, Context ctx){
            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
            // generate random color
            int color1 = generator.getRandomColor();

            String name = Character.toString(uName.charAt(0)).toUpperCase();

            TextDrawable drawable = TextDrawable.builder() .beginConfig()
                    .width(60)
                    .height(60)
                    .endConfig()
                    .buildRound(Character.toString(name.charAt(0)).toUpperCase(), color1);

            userImageView = (CircleImageView) mView.findViewById(R.id.user_single_image);

            Picasso.with(ctx).load(thumb_image).networkPolicy(NetworkPolicy.OFFLINE).placeholder(drawable).into(userImageView);

        }
        public void setUserOnline(String online_status){

            ImageView userOnlineView = (ImageView) mView.findViewById(R.id.user_single_online_icon);

            if(online_status.equals("true") || online_status.equals("onlineTyping")){
                userOnlineView.setVisibility(View.VISIBLE);
            }else{
                userOnlineView.setVisibility(View.INVISIBLE);
            }

        }

    }


    }
