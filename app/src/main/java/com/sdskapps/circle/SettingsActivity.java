package com.sdskapps.circle;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jaredrummler.cyanea.app.CyaneaAppCompatActivity;
import com.sdskapps.circle.CircleRectTransition.CircleRectView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.polaric.colorful.CActivity;
import org.polaric.colorful.Colorful;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import id.zelory.compressor.Compressor;
import jp.wasabeef.picasso.transformations.BlurTransformation;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SettingsActivity extends CyaneaAppCompatActivity {

    private DatabaseReference mUserDatabase;
    private FirebaseUser mCurrentUser;

    //Android Layout
    private RelativeLayout mLinearLayout;
    private int color=Colorful.getThemeDelegate().getPrimaryColor().getColorRes();
    private int accentColor=Colorful.getThemeDelegate().getAccentColor().getColorRes();
    private CircleRectView mDisplayImage;
    private TextView mName;
    private TextView mStatus;

    private ImageView bgBackground;

    private Button mStatusBtn;
    private Button mImageBtn;
    private Button mAppSettings;
    private FloatingActionButton fab;

    private static final int GALLERY_PICK=1;

    //Storage Firebase
    private StorageReference mImageStorage;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);


        //----------Colorful-----------------

        //-------------Colorful-------

        final Transformation transformation = new BlurTransformation(getApplicationContext(),50);

        mDisplayImage=(CircleRectView) findViewById(R.id.settings_image);
        mName=(TextView) findViewById(R.id.settings_display_name);
        mStatus=(TextView) findViewById(R.id.settings_status);
        mStatusBtn=(Button)findViewById(R.id.settings_status_btn);
        mImageBtn=(Button) findViewById(R.id.settings_image_btn) ;
        bgBackground = (ImageView) findViewById(R.id.settings_bg_image);
        mAppSettings = (Button) findViewById(R.id.btn_AppSettings);
        fab = (FloatingActionButton) findViewById(R.id.fab_profileImage);
        mImageStorage= FirebaseStorage.getInstance().getReference();
            //------------------Colorful Implementation------------------------
            mLinearLayout=(RelativeLayout) findViewById(R.id.settings_layout);
            mLinearLayout.setBackgroundColor(getResources().getColor(color));
            mImageBtn.setBackgroundColor(getResources().getColor(color));
            mStatusBtn.setBackgroundColor(getResources().getColor(accentColor));
            //------------------Colorful Implementation------------------------
        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        String current_uid= mCurrentUser.getUid();
        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(current_uid);
        mUserDatabase.keepSynced(true);


        mUserDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String name= dataSnapshot.child("name").getValue().toString();
                final String image= dataSnapshot.child("image").getValue().toString();
                String status= dataSnapshot.child("status").getValue().toString();
                String thumb_image= dataSnapshot.child("thumb_image").getValue().toString();

                mName.setText(name);
                mStatus.setText(status);

                if(!image.equals("default")) {

                    mDisplayImage.setOnClickListener((View v) ->{
                        Intent intent = new Intent(SettingsActivity.this, MessageImageViewer.class);
                        intent.putExtra("messageImage",image);
                        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SettingsActivity.this, (View)mDisplayImage, getString(R.string.transition));

                        ActivityCompat.startActivity(SettingsActivity.this, intent , transitionActivityOptions.toBundle());

//                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(SettingsActivity.this,mDisplayImage,"msgImage");
//                        startActivity(intent,options.toBundle());
                    });

                   // Picasso.with(SettingsActivity.this).load(image).placeholder(R.drawable.default_avatar1).into(mDisplayImage);
                    Picasso.with(SettingsActivity.this).load(image).networkPolicy(NetworkPolicy.OFFLINE)
                            .placeholder(R.drawable.default_avatar1).into(mDisplayImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            }

                        @Override
                        public void onError() {

                            Picasso.with(SettingsActivity.this).load(image).placeholder(R.drawable.default_avatar1).into(mDisplayImage);
                        }
                    });

                    Picasso.with(SettingsActivity.this).load(thumb_image).transform(transformation)
                            .transform(new RoundedCornersTransformation(15,0, RoundedCornersTransformation.CornerType.ALL))
                            .networkPolicy(NetworkPolicy.OFFLINE).into(bgBackground, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Picasso.with(SettingsActivity.this).load(thumb_image)
                                    .transform(transformation)
                                    .transform(new RoundedCornersTransformation(15,0, RoundedCornersTransformation.CornerType.ALL))
                                    .into(bgBackground);
                        }
                    });

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mAppSettings.setOnClickListener( v-> {
            startActivity(new Intent(SettingsActivity.this,AppSettings.class));
        });

        mStatusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String status_value=mStatus.getText().toString();
                Intent status_intent=new Intent(SettingsActivity.this,StatusActivity.class);
                status_intent.putExtra("status_value",status_value);
                startActivity(status_intent);
            }
        });


        mImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent galleryIntent = new Intent();
                galleryIntent.setType("image/*");
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(galleryIntent,"SELECT IMAGE"),GALLERY_PICK);

             /*   CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(SettingsActivity.this); */

            }
        });

        //------------FAB implementation----------------
        fab.setOnClickListener( v -> {
            CharSequence options[] = new CharSequence[]{"Change","Remove"};
            AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
            builder.setTitle("Profile Photo");
            builder.setItems(options, (dialogInterface,pos) -> {

                if(pos == 0){
                    CropImage.activity()
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setAspectRatio(1,1)
                            .setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                            .setOutputCompressQuality(50)
                            .start(SettingsActivity.this);
                }
                if(pos == 1){
                    Map update_hashMap=new HashMap<String, String>();
                    update_hashMap.put("image","default");
                    update_hashMap.put("thumb_image","default");
                    mUserDatabase.updateChildren(update_hashMap).addOnCompleteListener(task -> {
                        if(task.isSuccessful()){
                            Picasso.with(SettingsActivity.this).load(R.drawable.default_avatar1).into(mDisplayImage);
                            Toast.makeText(SettingsActivity.this, "Successfully Removed", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(SettingsActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            });
            builder.show();
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY_PICK && resultCode == RESULT_OK){

            Uri imageUri = data.getData();



            CropImage.activity(imageUri)
                    .setAspectRatio(1,1)
                    .setOutputCompressQuality(75)  //Can't confirm if it works or not
                    .start(this);

           //

        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {


                mProgressDialog=new ProgressDialog(SettingsActivity.this);
                mProgressDialog.setTitle("Uploading Image");
                mProgressDialog.setMessage("Please wait the image is being uploaded.");
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
                Uri resultUri = result.getUri();

                File thumb_filePath=new File(resultUri.getPath());


                String current_user_id=mCurrentUser.getUid();
                Bitmap thumb_bitmap;
                thumb_bitmap=null;
                try {
                        thumb_bitmap = new Compressor(this)
                                .setMaxHeight(200)
                                  .setMaxWidth(200)
                                  .setQuality(50)
                                  .compressToBitmap(thumb_filePath);

                    }catch (java.io.IOException e){

}
                ByteArrayOutputStream baos= new ByteArrayOutputStream();
                thumb_bitmap.compress(Bitmap.CompressFormat.JPEG,100,baos);
                final byte[] thumb_byte=baos.toByteArray();



                StorageReference filepath = mImageStorage.child("profile_images").child(current_user_id+".jpg");
                final StorageReference thumb_Path=mImageStorage.child("profile_images").child("thumbs").child(current_user_id+".jpg");

                filepath.putFile(resultUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                       // String download_url = taskSnapshot.getDownloadUrl().toString();
                        filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {

                                String download_url = uri.toString();

                                UploadTask uploadTask= thumb_Path.putBytes(thumb_byte);
                                uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot thumb_task) {
                                        //String thumb_downloadUrl = thumb_task.getMetadata().getReference().getDownloadUrl().toString();

                                        thumb_Path.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                            @Override
                                            public void onSuccess(Uri thumb_uri) {

                                                String thumb_downloadUrl = thumb_uri.toString();

                                                Map update_hashMap=new HashMap<String, String>();
                                                update_hashMap.put("image",download_url);
                                                update_hashMap.put("thumb_image",thumb_downloadUrl);
                                                mUserDatabase.updateChildren(update_hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if(task.isSuccessful()){
                                                            mProgressDialog.dismiss();
                                                            Toast.makeText(SettingsActivity.this,"Successfully Uploaded",Toast.LENGTH_SHORT).show();
                                                        }
                                                        else{
                                                            Toast.makeText(SettingsActivity.this, "Cannnot upload!", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                            }
                                        });

                            }
                        });

                            }
                        });
                    }
                });

                   /*     .addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                        if(task.isSuccessful())
                        {
                           // Toast.makeText(SettingsActivity.this,"Working",Toast.LENGTH_LONG).show();
                            @SuppressWarnings("VisibleForTests") final
                            String download_url=task.getResult().getMetadata().getReference().getDownloadUrl().toString();

                            UploadTask uploadTask= thumb_Path.putBytes(thumb_byte);
                            uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> thumb_task) {
                                    String thumb_downloadUrl= thumb_task.getResult().getMetadata().getReference().getDownloadUrl().toString();
                                    if(thumb_task.isSuccessful()){

                                       Map update_hashMap=new HashMap<String, String>();
                                        update_hashMap.put("image",download_url);
                                        update_hashMap.put("thumb_image",thumb_downloadUrl);
                                        mUserDatabase.updateChildren(update_hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if(task.isSuccessful()){
                                                    mProgressDialog.dismiss();
                                                    Toast.makeText(SettingsActivity.this,"Successfully Uploaded",Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                                    }

                                }
                            });


                        }
                        else{
                            Toast.makeText(SettingsActivity.this,"Error in uploading",Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();
                        }

                    }
                });  */


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public static String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(10);
        char tempChar;
        for (int i = 0; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    public void thumb () throws IOException{

 
   }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
