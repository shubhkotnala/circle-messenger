package com.sdskapps.circle;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.FloatRange;
import androidx.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import agency.tango.materialintroscreen.MaterialIntroActivity;
import agency.tango.materialintroscreen.SlideFragmentBuilder;
import agency.tango.materialintroscreen.animations.IViewTranslation;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by shubhkotnala on 25/12/17.
 */

public class IntroActivity extends MaterialIntroActivity {

    private int confusedEmoji = 0x1F605;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enableLastSlideAlphaExitTransition(true);

        getBackButtonTranslationWrapper()
                .setEnterTranslation(new IViewTranslation() {
                    @Override
                    public void translate(View view, @FloatRange(from = 0, to = 1.0) float percentage) {
                        view.setAlpha(percentage);
                    }
                });

        addSlide(new SlideFragmentBuilder()
        .backgroundColor(R.color.introFirst)
                .buttonsColor(R.color.md_yellow_800)
                .title("Circle Messenger")
                .image(R.drawable.circle_img)
                .description("Circle is an Instant messenger\n" +
                        "No fancy stuff "+getEmoji(0x1F613)+"\n" +
                        "No killer features " + getEmoji(0x1F614) +
                        "\nYet a simple messenger " + getEmoji(0x1F605))
        .build());

        addSlide(new SlideFragmentBuilder()
                .backgroundColor(R.color.md_green_500)
                .buttonsColor(R.color.yellow)
                .title("Features?")
                .image(R.drawable.circle_img)
                .description("No fancy features, just some basic stuff\n" +
                        "Messages can be sent (Text + Images) \n" +
                        "Not much beautiful UI but a simple, easy to navigate UI " +
                        "\nThemes! you have a range of 16x16 colors to select for the UI" + getEmoji(0x1F605))
                .build());


        addSlide(new SlideFragmentBuilder()
                        .backgroundColor(R.color.red)
                        .buttonsColor(R.color.bGreen)
                        .neededPermissions(new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
                        .image(agency.tango.materialintroscreen.R.drawable.ic_next)
                        .title("Permissions")
                        .description("Please allow these permissions to make the app work properly. " +
                                "\nPermission details\n" +
                                "1. Camera : To capture images (obviously) " + getEmoji(0x1F605) +
                        "\n2. Storage : We need to have read access to your storage to send images to your chats and to setup your dp " + getEmoji(0x1F603)+
                        "\n\nWhat more permission do we take?\nInternet : this is the most important permission without which the app would be useless but this is already granted " + getEmoji(0x1F638))
                        .build()); //,
            //    new MessageButtonBehaviour(new View.OnClickListener() {
                //    @Override
              //      public void onClick(View v) {
              //          showMessage("Enjoy using Circle Messenger.\nDo note that this is pre-release version so you experience some bugs.");
                //    }
                //}, "Enjoy!"));
        addSlide(new SlideFragmentBuilder()
                .backgroundColor(R.color.bGreen)
                .buttonsColor(R.color.profileBg)
                .title("You're ready to Rock!")
                .description("Enjoy your new personal Messenger\nMade with " + getEmoji(0x2665) + " and support of family and friends" + "\n\n\n Copyright " +
                        getEmoji(0x00A9)+" SDSK Apps")
        .build());

    }

    @Override
    public void onFinish() {
        super.onFinish();
        Toast.makeText(getApplicationContext(),"Enjoy using Circle Messenger.\nDo note that this is pre-release version so you may experience some bugs.",Toast.LENGTH_LONG).show();
        SharedPreferences settings=getSharedPreferences("firstCircleRun",0);
        SharedPreferences.Editor editor=settings.edit();
        editor.putBoolean("firstRun",true);
        editor.commit();
        startActivity(new Intent(IntroActivity.this,MainActivity.class));
    }
    public String getEmoji(int unicode){
        return new String(Character.toChars(unicode));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
