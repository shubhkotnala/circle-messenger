package com.sdskapps.circle.CircleRectTransition;

import android.animation.Animator;

/**
 * Created by shubhkotnala on 29/4/18.
 */

public abstract class AbstractAnimatorListener implements Animator.AnimatorListener {
    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {

    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }
}