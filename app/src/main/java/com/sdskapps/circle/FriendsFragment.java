package com.sdskapps.circle;


import android.app.ActivityOptions;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.polaric.colorful.Colorful;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FriendsFragment extends Fragment {

    private int primary= Colorful.getThemeDelegate().getPrimaryColor().getColorRes();
    private RecyclerView mFriendsList;
    private DatabaseReference mFriendsDatabase;
    private DatabaseReference mUsersDatabase;
    private FirebaseAuth mAuth;

    private String mCurrent_user_id;

    private View mMainView;
    private String imageUri = null;


    public FriendsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mMainView = inflater.inflate(R.layout.fragment_friends, container, false);

        mFriendsList =(RecyclerView) mMainView.findViewById(R.id.friends_list);
        mAuth = FirebaseAuth.getInstance();

        mCurrent_user_id = mAuth.getCurrentUser().getUid();

        mFriendsDatabase = FirebaseDatabase.getInstance().getReference().child("Friends").child(mCurrent_user_id);
       // mFriendsDatabase.keepSynced(true);
        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
       // mUsersDatabase.keepSynced(true);

        mFriendsList.setHasFixedSize(true);
        mFriendsList.setLayoutManager(new LinearLayoutManager(getContext()));
        Log.d("Color",":"+primary);
        return mMainView;
    }

    @Override
    public void onStart() {
        super.onStart();

        final FirebaseRecyclerAdapter<Friends, FriendsViewHolder> friendsRecyclerViewAdapter= new FirebaseRecyclerAdapter<Friends, FriendsViewHolder>(
                Friends.class,
                R.layout.users_single_layout,
                FriendsViewHolder.class,
                mFriendsDatabase
        ) {
            @Override
            protected void populateViewHolder(final FriendsViewHolder viewHolder, Friends friends, int i) {

                viewHolder.setDate(friends.getDate());
                final String list_user_id=getRef(i).getKey();
                mUsersDatabase.child(list_user_id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot dataSnapshot) {

                        final String userName= dataSnapshot.child("name").getValue().toString();
                         final String userThumb= dataSnapshot.child("thumb_image").getValue().toString();
                        final String status = dataSnapshot.child("status").getValue().toString();
                        imageUri = dataSnapshot.child("image").getValue().toString();
                        if(dataSnapshot.hasChild("online")) {
                            String userOnline =dataSnapshot.child("online").getValue().toString();
                            viewHolder.setUserOnline(userOnline);
                        }

                        viewHolder.setName(userName);
                        viewHolder.setUserImage(userName,userThumb,getContext());

                        viewHolder.userImageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                viewHolder.userImageView.setTransitionName("msgImage");
                                Intent messageImageIntent = new Intent(getContext(),MessageImageViewer.class);
                                messageImageIntent.putExtra("messageImage",dataSnapshot.child("image").getValue().toString());
                                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(),viewHolder.userImageView,"msgImage");
                                startActivity(messageImageIntent,options.toBundle());
                                final String[] resources = new String[]{
                                  dataSnapshot.child("image").getValue().toString()
                                };
                            /*    Intent a = new Intent(getContext(), StatusStoriesActivity.class);
                                a.putExtra(StatusStoriesActivity.STATUS_RESOURCES_KEY, resources);
                                a.putExtra(StatusStoriesActivity.STATUS_DURATION_KEY, 3000L);
                                a.putExtra(StatusStoriesActivity.IS_IMMERSIVE_KEY, true);
                                a.putExtra(StatusStoriesActivity.IS_CACHING_ENABLED_KEY, true);
                                a.putExtra(StatusStoriesActivity.IS_TEXT_PROGRESS_ENABLED_KEY, true);
                                startActivity(a);  */
                            }
                        });


                        viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                CharSequence options[]=new CharSequence[]{"Open Profile", "Send Message"};
                                AlertDialog.Builder builder= new AlertDialog.Builder(getContext());
                                builder.setTitle("Select Option");
                                builder.setItems(options, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        //Click Events
                                        if(i == 0){
                                            Intent profileIntent= new Intent(getContext(),ProfileActivity.class);
                                            profileIntent.putExtra("user_id",list_user_id);
                                            profileIntent.putExtra("color",primary);
                                            startActivity(profileIntent);

                                        }
                                        if(i == 1){
                                            //Chats
                                            Intent chatIntent= new Intent(getContext(),ChatActivity.class);
                                            chatIntent.putExtra("user_id",list_user_id);
                                            chatIntent.putExtra("user_name",userName);
                                            chatIntent.putExtra("color",primary);
                                            chatIntent.putExtra("status",status);
                                            startActivity(chatIntent);

                                        }

                                    }
                                });
                                builder.show();
                            }
                        });

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        };
        mFriendsList.setAdapter(friendsRecyclerViewAdapter);
    }

    public static class FriendsViewHolder extends RecyclerView.ViewHolder {

        View mView;
        CircleImageView userImageView;
        public FriendsViewHolder(View itemView){
            super(itemView);
            mView=itemView;
        }
        public void setDate(String date){
            TextView userStatusView= (TextView) mView.findViewById(R.id.user_single_status);
            userStatusView.setText(date+" ");
        }
        public void setName(String name){
            TextView userNameView = (TextView) mView.findViewById(R.id.user_single_name);
            userNameView.setText(name+" ");
        }
        public void setUserImage(String uName,String thumb_image, Context ctx){
            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
            // generate random color
            int color1 = generator.getRandomColor();

            String name = Character.toString(uName.charAt(0)).toUpperCase();

            TextDrawable drawable = TextDrawable.builder() .beginConfig()
                    .width(60)
                    .height(60)
                    .endConfig()
                    .buildRound(Character.toString(name.charAt(0)).toUpperCase(), color1);

            userImageView = (CircleImageView) mView.findViewById(R.id.user_single_image);

            Picasso.with(ctx).load(thumb_image).networkPolicy(NetworkPolicy.OFFLINE)
                    .placeholder(drawable).into(userImageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Picasso.with(ctx).load(thumb_image).placeholder(drawable).into(userImageView);
                }
            });

        }
        public void setUserOnline(String online_status){

            CircleImageView userOnlineView = (CircleImageView) mView.findViewById(R.id.user_single_online_icon);

            if(online_status.equals("true") || online_status.equals("onlineTyping")){
                userOnlineView.setVisibility(View.VISIBLE);
            }else{
                userOnlineView.setVisibility(View.INVISIBLE);
            }

        }

    }
}
