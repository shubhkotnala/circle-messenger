package com.sdskapps.circle;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.polaric.colorful.Colorful;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class RequestsFragment extends Fragment {


    private int primary = Colorful.getThemeDelegate().getPrimaryColor().getColorRes();
    private RecyclerView mFriendsList;
    private DatabaseReference mFriendsDatabase;
    private DatabaseReference mUsersDatabase;
    private FirebaseAuth mAuth;

    private String mCurrent_request_id;

    private View mMainView;
    private String imageUri = null;

    private LinearLayout emptyLayout;


    public RequestsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mMainView = inflater.inflate(R.layout.fragment_requests, container, false);

        mFriendsList = (RecyclerView) mMainView.findViewById(R.id.requests_list);
        mAuth = FirebaseAuth.getInstance();

        emptyLayout = mMainView.findViewById(R.id.requestEmptyLayout);

        emptyLayout.setVisibility(View.VISIBLE);

        final ImageView animatingImage = (ImageView) mMainView.findViewById(R.id.animatedImg);
        //Glide.with(RequestsFragment.this).load(R.drawable.sleeping).asGif().crossFade().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(animatingImage);

       // mMainView.findViewById(R.id.requestEmptyLayout).setVisibility(View.GONE);

        mCurrent_request_id = mAuth.getCurrentUser().getUid();

        mFriendsDatabase = FirebaseDatabase.getInstance().getReference().child("Friend_req").child(mCurrent_request_id);
        mFriendsDatabase.keepSynced(true);
        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mUsersDatabase.keepSynced(true);

        mFriendsList.setHasFixedSize(true);
        mFriendsList.setLayoutManager(new LinearLayoutManager(getContext()));
        Log.d("Color", ":" + primary);
        return mMainView;
    }

    @Override
    public void onStart() {
        super.onStart();

        final FirebaseRecyclerAdapter<Friends, RequestsFragment.RequestViewHolder> requestsRecyclerViewAdapter = new FirebaseRecyclerAdapter<Friends, RequestViewHolder>(
                Friends.class,
                R.layout.requests_single_layout,
                RequestsFragment.RequestViewHolder.class,
                mFriendsDatabase
        ) {
            @Override
            protected void populateViewHolder(final RequestsFragment.RequestViewHolder viewHolder, Friends friends, int i) {

                viewHolder.setDate(friends.getDate());
                 final String list_request_id = getRef(i).getKey();
                    emptyLayout.setVisibility(View.GONE);
                mUsersDatabase.child(list_request_id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        final String requestName = dataSnapshot.child("name").getValue().toString();
                        String requestThumb = dataSnapshot.child("thumb_image").getValue().toString();
                        final String status = dataSnapshot.child("status").getValue().toString();
                        imageUri = dataSnapshot.child("image").getValue().toString();
                        if (dataSnapshot.hasChild("online")) {
                            String requestOnline = dataSnapshot.child("online").getValue().toString();
                            viewHolder.setUserOnline(requestOnline);
                        }

                       final String uid = list_request_id;

                        viewHolder.setName(requestName);
                        viewHolder.setDate(status);
                        viewHolder.setUserImage(requestName, requestThumb, getContext());

                    /*    viewHolder.requestImageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent messageImageIntent = new Intent(viewHolder.requestImageView.getContext(), MessageImageViewer.class);
                                messageImageIntent.putExtra("messageImage", imageUri);
                                viewHolder.requestImageView.getContext().startActivity(messageImageIntent);
                            }
                        });  */


                        viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent profileIntent = new Intent(getContext(),ProfileActivity.class);
                                profileIntent.putExtra("user_id",list_request_id);
                                startActivity(profileIntent);

                          /*      CharSequence options[] = new CharSequence[]{"Open Profile", "Send Message"};
                                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                builder.setTitle("Select Option");
                                builder.setItems(options, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        //Click Events
                                        if (i == 0) {
                                            Intent profileIntent = new Intent(getContext(), ProfileActivity.class);
                                            profileIntent.putExtra("user_id", uid);
                                            profileIntent.putExtra("color", primary);
                                            startActivity(profileIntent);

                                        }
                                        if (i == 1) {
                                            //Chats
                                            Intent chatIntent = new Intent(getContext(), ChatActivity.class);
                                            chatIntent.putExtra("request_id", list_request_id);
                                            chatIntent.putExtra("request_name", requestName);
                                            chatIntent.putExtra("color", primary);
                                            startActivity(chatIntent);

                                        }

                                    }
                                });
                                builder.show();  */
                            }
                        });

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        };
        mFriendsList.setAdapter(requestsRecyclerViewAdapter);

        if(requestsRecyclerViewAdapter.getItemCount() == 0){

            LinearLayout emptyLayout = (LinearLayout) mMainView.findViewById(R.id.requestEmptyLayout);
         //   emptyLayout.setVisibility(View.VISIBLE);

        } else {

            LinearLayout emptyLayout = (LinearLayout) mMainView.findViewById(R.id.requestEmptyLayout);
            emptyLayout.setVisibility(View.GONE);

        }
    }

    public static class RequestViewHolder extends RecyclerView.ViewHolder {

        View mView;
        CircleImageView requestImageView;

        public RequestViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setDate(String date) {
            TextView requestStatusView = (TextView) mView.findViewById(R.id.request_single_status);
            requestStatusView.setText(date + " ");
        }

        public void setName(String name) {
            TextView requestNameView = (TextView) mView.findViewById(R.id.request_single_name);
            requestNameView.setText(name + " ");
        }

        public void setUserImage(String uName, String thumb_image, Context ctx) {
            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
            // generate random color
            int color1 = generator.getRandomColor();

            String name = Character.toString(uName.charAt(0)).toUpperCase();

            TextDrawable drawable = TextDrawable.builder().beginConfig()
                    .width(60)
                    .height(60)
                    .endConfig()
                    .buildRound(Character.toString(name.charAt(0)).toUpperCase(), color1);

            requestImageView = (CircleImageView) mView.findViewById(R.id.request_single_image);

            Picasso.with(ctx).load(thumb_image).networkPolicy(NetworkPolicy.OFFLINE)
                    .placeholder(drawable).into(requestImageView, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Picasso.with(ctx).load(thumb_image).placeholder(drawable).into(requestImageView);
                }
            });

        }

        public void setUserOnline(String online_status) {

            CircleImageView requestOnlineView = (CircleImageView) mView.findViewById(R.id.request_single_online_icon);

            if (online_status.equals("true") || online_status.equals("onlineTyping")) {
                requestOnlineView.setVisibility(View.VISIBLE);
            } else {
                requestOnlineView.setVisibility(View.INVISIBLE);
            }

        }

    }
}
