package com.sdskapps.circle;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.polaric.colorful.CActivity;
import org.polaric.colorful.Colorful;

import java.util.Arrays;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class StartActivity extends CActivity {

    private Button mRegBtn;
    private Button mLoginBtn;
    private RelativeLayout mRelativeLayout;
    private Button mPhoneAuthBtn;
    private int color= Colorful.getThemeDelegate().getPrimaryColor().getColorRes();
    private View mRootView;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        mRootView = findViewById(R.id.start_layout);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Users");

        mRegBtn=(Button) findViewById(R.id.start_reg_button);
        mLoginBtn=(Button) findViewById(R.id.start_login_btn);
        mPhoneAuthBtn = (Button) findViewById(R.id.start_phone_auth_button);
        mRelativeLayout = (RelativeLayout) findViewById(R.id.start_layout);
     /*   mPhoneAuthBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(StartActivity.this,PhoneAuthActivity.class));
                finish();

            }
        }); */
        mRelativeLayout.setBackgroundColor(getResources().getColor(color));

        mRegBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent reg_intent= new Intent(StartActivity.this, RegisterActivity.class);
                startActivity(reg_intent);
            }
        });

        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent login_intetn= new Intent(StartActivity.this, LoginActivity.class);
                startActivity(  login_intetn);
            }
        });
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 777) {
            handleSignInResponse(resultCode, data);
        }
    }

    private void handleSignInResponse(int resultCode, Intent data) {
        IdpResponse response = IdpResponse.fromResultIntent(data);

        if (resultCode == RESULT_OK) {
           /* final String phone = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber();
            final String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();

            mDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(!dataSnapshot.hasChild(uid)){

                        Intent shownameIntent = new Intent(StartActivity.this,ShowNameDialog.class);
                        shownameIntent.putExtra("uid",uid);
                        shownameIntent.putExtra("phone",phone);
                        shownameIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(shownameIntent);


                    } else {

                    goMainScreen();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
*/
           goMainScreen();

        } else {
            if (response == null) {
                showSnackbar(R.string.sign_in_cancelled);
                return;
            }
            if (response.getErrorCode() == ErrorCodes.NO_NETWORK) {
                showSnackbar(R.string.no_internet_connection);
                return;
            }
            if (response.getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                showSnackbar(R.string.unknown_error);
                return;
            }
        }
        showSnackbar(R.string.unknown_sign_in_response);
    }

    public void signInPhone(View view) {
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(
                                Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.PHONE_VERIFICATION_PROVIDER).build()))
                        .build(),
                777);

    }

    private void goMainScreen() {
       /* Intent intent = new Intent(this, MainActivity.class); */
       Intent intent = new Intent(this,ShowNameDialog.class);
        final String phone = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber();
        final String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        intent.putExtra("uid",uid);
        intent.putExtra("phone",phone);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void showSnackbar(int errorMessageRes) {
        Snackbar.make(mRootView, errorMessageRes, Snackbar.LENGTH_LONG).show();
    }

}
