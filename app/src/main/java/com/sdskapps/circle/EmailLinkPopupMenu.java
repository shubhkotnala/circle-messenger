package com.sdskapps.circle;


import android.content.Context;
import android.view.View;
import android.widget.PopupMenu;


/**
 * Created by shubhkotnala on 17/3/18.
 */

public class EmailLinkPopupMenu extends PopupMenu {

    public EmailLinkPopupMenu(Context context, View anchor) {
        super(context, anchor);
        getMenuInflater().inflate(R.menu.email_link_menu, getMenu());
    }
}