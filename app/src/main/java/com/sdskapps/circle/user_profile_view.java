package com.sdskapps.circle;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * Created by Shubhankar on 8/24/2017.
 */


public class user_profile_view extends AppCompatActivity {

    private Toolbar mToolbar;
    private String userName;
    private String mChatUser;
    private ImageView mProfileImage;
    private DatabaseReference mRootRef;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private AppBarLayout appBar;
    private String imageUri = null;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme1);
        setContentView(R.layout.user_profile_view);

       final Window w = getWindow(); // in Activity's onCreate() for instance
       // w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        mToolbar = (Toolbar) findViewById(R.id.anim_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setTitle("Title here");
        setTitle("Title here");
        userName = getIntent().getStringExtra("user_name");
        mChatUser = getIntent().getStringExtra("user_id");
        mProfileImage = (ImageView) findViewById(R.id.profile_image);
        //getSupportActionBar().setTitle(userName);
        setTitle(userName);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        appBar = (AppBarLayout) findViewById(R.id.appbar);

        mRootRef = FirebaseDatabase.getInstance().getReference();
        mRootRef.child("Users").child(mChatUser).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                final String image = dataSnapshot.child("image").getValue().toString();
                imageUri = image;
                if(!image.equals("default")) {

                    Picasso.with(user_profile_view.this).load(image).networkPolicy(NetworkPolicy.OFFLINE)
                            .placeholder(R.drawable.default_avatar1).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            assert mProfileImage != null;
                            mProfileImage.setImageBitmap(bitmap);
                            if(bitmap != null){
                                Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, 1, 1, true);
                                final int color = newBitmap.getPixel(0, 0);
                                Boolean dark = isColorDark(color);
                                if(dark){
                                    mToolbar.setTitleTextColor(Color.WHITE);
                                    collapsingToolbarLayout.setCollapsedTitleTextColor(ColorStateList.valueOf(Color.WHITE));
                                    setTitleColor(Color.WHITE);
                                    mToolbar.getNavigationIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                                    collapsingToolbarLayout.setExpandedTitleTextColor(ColorStateList.valueOf(Color.WHITE));
                                } else {
                                    mToolbar.setTitleTextColor(Color.BLACK);
                                    collapsingToolbarLayout.setCollapsedTitleTextColor(Color.BLACK);
                                    collapsingToolbarLayout.setExpandedTitleTextColor(ColorStateList.valueOf(Color.BLACK));
                                    mToolbar.getNavigationIcon().setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
                                    /*appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                                        @Override
                                        public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                                            if ((collapsingToolbarLayout.getHeight() + verticalOffset) < (2 * ViewCompat.getMinimumHeight(collapsingToolbarLayout))) {
                                                mToolbar.getNavigationIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                                            } else {
                                                mToolbar.getNavigationIcon().setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
                                            }
                                        }
                                    }); */
                                }
                                collapsingToolbarLayout.setBackgroundColor(color);
                                collapsingToolbarLayout.setStatusBarScrimColor(color);
                                collapsingToolbarLayout.setContentScrimColor(color);
                                w.setStatusBarColor(color);
                                Toast.makeText(user_profile_view.this,"Color set successfully",Toast.LENGTH_LONG).show();
                                newBitmap.recycle();
                            }

                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent imageIntent = new Intent(user_profile_view.this,MessageImageViewer.class);
                imageIntent.putExtra("messageImage",imageUri);
                startActivity(imageIntent);
            }
        });

    }

    public boolean isColorDark(int color){
        double darkness = 1-(0.299* Color.red(color) + 0.587*Color.green(color) + 0.114*Color.blue(color))/255;
        return darkness >= 0.5;
    }

}
