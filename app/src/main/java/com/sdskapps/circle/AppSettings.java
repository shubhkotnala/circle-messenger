package com.sdskapps.circle;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.jaredrummler.cyanea.app.CyaneaAppCompatActivity;

import org.polaric.colorful.CActivity;
import org.polaric.colorful.ColorPickerDialog;
import org.polaric.colorful.Colorful;

public class AppSettings extends CyaneaAppCompatActivity {

    private CheckedTextView roundedNotification,largerNotification;
    private TextView primaryColor, accentColor,appFont;
    private Button btnTestNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_settings);

        roundedNotification = (CheckedTextView) findViewById(R.id.btn_roundedNotificaiton);
        largerNotification = (CheckedTextView) findViewById(R.id.btn_largerNotification);
        primaryColor = (TextView) findViewById(R.id.settings_primaryColor);
        accentColor = (TextView) findViewById(R.id.settings_accentColor);
        btnTestNotification = (Button) findViewById(R.id.btn_testNotification);
        appFont = (TextView) findViewById(R.id.settings_appFont);

        SharedPreferences notificationSettings=getSharedPreferences("notifications",0);
        SharedPreferences.Editor editor = notificationSettings.edit();

        Boolean roundedDefault = notificationSettings.getBoolean("CircleNotification",false);
        Boolean largerDefault = notificationSettings.getBoolean("CircleNotificationLarger",false);

        roundedNotification.setChecked(roundedDefault);
        largerNotification.setChecked(largerDefault);

        primaryColor.setOnClickListener( v -> {

            ColorPickerDialog dialog = new ColorPickerDialog(AppSettings.this);
            dialog.setTitle("Accent Color");
            dialog.setOnColorSelectedListener(new ColorPickerDialog.OnColorSelectedListener() {
                @Override
                public void onColorSelected(Colorful.ThemeColor colorFul) {
                    //TODO: Do something with the color
                    Colorful.config(AppSettings.this)
                            .primaryColor(colorFul)
                            .translucent(false)
                            .dark(false)
                            .apply();
                    recreate();
                }
            });
            dialog.show();

        });

        accentColor.setOnClickListener( v -> {
            ColorPickerDialog dialog = new ColorPickerDialog(AppSettings.this);
            dialog.setTitle("Accent Color");
            dialog.setOnColorSelectedListener(new ColorPickerDialog.OnColorSelectedListener() {
                @Override
                public void onColorSelected(Colorful.ThemeColor colorFul) {
                    //TODO: Do something with the color

                    Colorful.config(AppSettings.this)
                            .accentColor(colorFul)
                            .translucent(false)
                            .dark(false)
                            .apply();
                    recreate();

                }
            });
            dialog.show();
        });

        appFont.setOnClickListener(v -> {
            startActivity(new Intent(AppSettings.this,FontsActivity.class));
        });

        roundedNotification.setOnClickListener( v -> {

            if(roundedNotification.isChecked()){
                editor.putBoolean("CircleNotification",false);
                editor.apply();
                roundedNotification.setChecked(false);
            }else{
                editor.putBoolean("CircleNotification",true);
                editor.apply();
                roundedNotification.setChecked(true);
            }

        });

        largerNotification.setOnClickListener( v -> {

            if(largerNotification.isChecked()){
                editor.putBoolean("CircleNotificationLarger",false);
                editor.apply();
                largerNotification.setChecked(false);
            }else{
                editor.putBoolean("CircleNotificationLarger",true);
                editor.apply();
                largerNotification.setChecked(true);
            }

        });

        btnTestNotification.setOnClickListener( v-> {

            AchievementUnlocked achievementUnlocked = new AchievementUnlocked(getApplicationContext());

            Boolean rounded = notificationSettings.getBoolean("CircleNotification",false);
            Boolean larger = notificationSettings.getBoolean("CircleNotificationLarger",false);

            achievementUnlocked.setRounded(rounded).setLarge(larger).setTopAligned(true).setDismissible(true);

            AchievementData data = new AchievementData();
            data.setTitle("Hello World");
            data.setSubtitle("This is a test notification");
            data.setBackgroundColor(getResources().getColor(Colorful.getThemeDelegate().getPrimaryColor().getColorRes()));
            data.setIcon(getDrawable(R.drawable.circle));
            data.setTextColor(Color.WHITE);
            achievementUnlocked.show(data);

        });

    }
}
