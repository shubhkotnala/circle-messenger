package com.sdskapps.circle;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import androidx.core.app.NotificationCompat;

import android.view.View;

import com.google.firebase.messaging.RemoteMessage;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.polaric.colorful.Colorful;

import java.io.IOException;


/**
 * Created by Shubhankar on 7/29/2017.
 */

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    private int color = Colorful.getThemeDelegate().getPrimaryColor().getColorRes();
    AchievementUnlocked achievementUnlocked;
    Intent resultIntent;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        PendingIntent resultPendingIntent = null;

        final String notification_title = remoteMessage.getData().get("title");
        String notification_body = remoteMessage.getData().get("body");
        String click_action = remoteMessage.getData().get("click_action");
        final String msgType = remoteMessage.getData().get("type");
        final String from_user_id = remoteMessage.getData().get("from_user_id");
        final String userStatus = remoteMessage.getData().get("status");
        final String image = remoteMessage.getData().get("thumb_image");

        final String type = remoteMessage.getData().get("type");
   /*     if(type != null && !type.trim().isEmpty() && type.equals("notification")){
            String body = remoteMessage.getNotification().getBody();
            String title = remoteMessage.getNotification().getTitle();
            Intent resultIntent = new Intent(this, MainActivity.class);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            resultPendingIntent =
                    PendingIntent.getActivity(
                            this,
                            (int) System.currentTimeMillis(),
                            resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(FirebaseMessagingService.this)
                            .setSmallIcon(R.drawable.circle)
                            .setContentTitle(notification_title)
                            .setContentText(notification_body)
                            .setPriority(Notification.PRIORITY_HIGH)
                            .setFullScreenIntent(resultPendingIntent,true)
                    .setDefaults(Notification.DEFAULT_ALL);
            NotificationManager mNotifyMgr =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);



            NotificationCompat.BigTextStyle bigText=new NotificationCompat.BigTextStyle();
            bigText.bigText(body);
            bigText.setBigContentTitle(title);
            Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + this.getPackageName() + "/" + R.raw.notification);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                AudioAttributes attributes = new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                        .build();
                NotificationChannel notificationChannel = new NotificationChannel("123432", "CircleNotification", importance);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.setSound(sound, attributes);
                notificationChannel.enableVibration(true);
                notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                assert mNotifyMgr != null;
                mBuilder.setChannelId("123432");
                mNotifyMgr.createNotificationChannel(notificationChannel);
            }
//Notification.InboxStyle inboxStyle =new Notification.InboxStyle();
            mBuilder.setContentIntent(resultPendingIntent);
            //  mBuilder.setFullScreenIntent(resultPendingIntent,true);
            mBuilder.setLights(Color.BLUE, 1500, 500);
            mBuilder.setSound(sound);
            mBuilder.setStyle(bigText);
            mBuilder.setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mBuilder.setColor(getResources().getColor(color));
            // Sets an ID for the notification
            int mNotificationId = (int) System.currentTimeMillis();
            // Gets an instance of the NotificationManager service

            // Builds the notification and issues it.
            mNotifyMgr.notify(mNotificationId, mBuilder.build());


        } else{

*/
        SharedPreferences chatUser = getSharedPreferences("chatDetails", 0);
        String chatUserId = chatUser.getString("chatUserId", "default");

        SharedPreferences notificationSettings=getSharedPreferences("notifications",0);
        Boolean rounded = notificationSettings.getBoolean("CircleNotification",false);
        Boolean larger = notificationSettings.getBoolean("CircleNotificationLarger",false);
        Bitmap bitmap = null;
        try {
            bitmap = Picasso.with(this)
                    .load(image).networkPolicy(NetworkPolicy.OFFLINE)
                    .get();
            if(bitmap == null){
                bitmap = Picasso.with(this)
                        .load(image)
                        .get();
            }

           // bitmap = getCircleBitmap(bitmap);
        }
        catch (IOException e){
            e.printStackTrace();
        }

        if (msgType.equals("message")) {
                     resultIntent = new Intent(FirebaseMessagingService.this, ChatActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    resultIntent.putExtra("user_id", from_user_id);
                    resultIntent.putExtra("user_name", notification_title);
                    resultIntent.putExtra("status",userStatus);
                    //startActivity(resultIntent);
                } else {
                     resultIntent = new Intent(FirebaseMessagingService.this, ProfileActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    resultIntent.putExtra("user_id", from_user_id);
                    resultIntent.putExtra("user_name", notification_title);
                    resultIntent.putExtra("status",userStatus);
//                    startActivity(resultIntent);
                }


        if (notification_body.startsWith("https://firebasestorage.googleapis.com")) {
            notification_body = "*Attachment*";
        } else if (notification_body.startsWith("load.sad.gif.here")) {
            notification_body = "*Sticker*";
        }
        if(!chatUserId.equals(from_user_id)){

            Drawable drawable = new BitmapDrawable(getResources(),bitmap);

        achievementUnlocked = new AchievementUnlocked(getApplicationContext());
        achievementUnlocked.setRounded(false).setLarge(false);
        achievementUnlocked.dismissWithoutAnimation();
        final AchievementData data = new AchievementData();
        data.setTitle(notification_title);
        data.setSubtitle(notification_body);
        data.setIcon(getResources().getDrawable(R.drawable.circle));
        //    data.setIcon(drawable);
        data.setTextColor(Color.WHITE);
        data.setIconBackgroundColor(getResources().getColor(color));
        data.setBackgroundColor(getResources().getColor(color));
        data.setPopUpOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(resultIntent);

//                if (msgType.equals("message")) {
//                    Intent resultIntent = new Intent(FirebaseMessagingService.this, ChatActivity.class);
//                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    resultIntent.putExtra("user_id", from_user_id);
//                    resultIntent.putExtra("user_name", notification_title);
//                    resultIntent.putExtra("status",userStatus);
//                    startActivity(resultIntent);
//                } else {
//                    Intent resultIntent = new Intent(FirebaseMessagingService.this, ProfileActivity.class);
//                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    resultIntent.putExtra("user_id", from_user_id);
//                    resultIntent.putExtra("user_name", notification_title);
//                    resultIntent.putExtra("status",userStatus);
//                    startActivity(resultIntent);
//                }


            }
        });

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {

                FirebaseMessagingService.this.achievementUnlocked.show(data);
            }
        });


            resultPendingIntent = PendingIntent.getActivity(this,
                    (int) System.currentTimeMillis(),
                    resultIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

//            if (msgType.equals("message")) {
//                Intent resultIntent = new Intent(this, ChatActivity.class);
//                resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                resultIntent.putExtra("user_id", from_user_id);
//                resultIntent.putExtra("user_name", notification_title);
//                resultIntent.putExtra("status",userStatus);
//                resultPendingIntent =
//                        PendingIntent.getActivity(
//                                this,
//                                (int) System.currentTimeMillis(),
//                                resultIntent,
//                                PendingIntent.FLAG_UPDATE_CURRENT
//                        );
//
//
//            } else {
//                Intent resultIntent = new Intent(this, ProfileActivity.class);
//                resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                resultIntent.putExtra("user_id", from_user_id);
//                resultIntent.putExtra("user_name", notification_title);
//                resultIntent.putExtra("status",userStatus);
//                resultPendingIntent =
//                        PendingIntent.getActivity(
//                                this,
//                                (int) System.currentTimeMillis(),
//                                resultIntent,
//                                PendingIntent.FLAG_UPDATE_CURRENT
//                        );
//
//
//            }


        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(FirebaseMessagingService.this)
                        .setSmallIcon(R.drawable.circle)

             //   .setLargeIcon(bitmap)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setFullScreenIntent(resultPendingIntent,true)


        // .setDefaults(Notification.DEFAULT_ALL)
          .setPriority(Notification.PRIORITY_HIGH);
            NotificationCompat.BigTextStyle bigText=new NotificationCompat.BigTextStyle();
            bigText.bigText(notification_body);
            bigText.setBigContentTitle(notification_title);


        Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + this.getPackageName() + "/" + R.raw.notification);

        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

 /*       NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
*/
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            NotificationChannel notificationChannel = new NotificationChannel("123432", "Circle", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setSound(sound, attributes);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotifyMgr != null;
            mBuilder.setChannelId("123432");
            mNotifyMgr.createNotificationChannel(notificationChannel);
        }
//Notification.InboxStyle inboxStyle =new Notification.InboxStyle();
        mBuilder.setContentIntent(resultPendingIntent);
        //  mBuilder.setFullScreenIntent(resultPendingIntent,true);
        mBuilder.setLights(Color.RED, 1500, 500);
        mBuilder.setSound(sound);
        mBuilder.setStyle(bigText);
         mBuilder.setContentTitle(notification_title)
                    .setContentText(notification_body);
        mBuilder.setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        mBuilder.setColor(getResources().getColor(color));
        // Sets an ID for the notification
        int mNotificationId = (int) System.currentTimeMillis();
        // Gets an instance of the NotificationManager service

        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());

        }
    }

    private Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        String deviceToken = s;
    }
}
