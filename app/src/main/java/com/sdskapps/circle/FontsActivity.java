package com.sdskapps.circle;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Process;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.cyanea.app.CyaneaAppCompatActivity;

import org.polaric.colorful.CActivity;

public class FontsActivity extends CyaneaAppCompatActivity {

    Button btnDancing,btnKaushan,btnPacifico,btnQuicksand,btnWalkway,btnDefault,btnSofia,btnBlackjack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fonts);

        btnDancing = (Button) findViewById(R.id.btnFontDancing);
        btnKaushan = (Button) findViewById(R.id.btnFontkaushan);
        btnPacifico = (Button) findViewById(R.id.btnFontPacifico);
        btnQuicksand = (Button) findViewById(R.id.btnFontQuicksand);
        btnWalkway = (Button) findViewById(R.id.btnFontWalkway);
        btnDefault = (Button) findViewById(R.id.btnFontDefault);
        btnSofia = (Button) findViewById(R.id.btnFontSofia);
        btnBlackjack = (Button) findViewById(R.id.btnFontBlackjack);

        TextView dancing = (TextView) findViewById(R.id.txtFontDancing);
        Typeface faceDance = Typeface.createFromAsset(getAssets(),
                "fonts/dacing.otf");
        dancing.setTypeface(faceDance);

        TextView kaushan = (TextView) findViewById(R.id.txtFontkaushan);
        Typeface faceKaushan = Typeface.createFromAsset(getAssets(),
                "fonts/kaushan.otf");
        kaushan.setTypeface(faceKaushan);

        TextView pacifico = (TextView) findViewById(R.id.txtFontPacifico);
        Typeface facePacifico = Typeface.createFromAsset(getAssets(),
                "fonts/pacifico.ttf");
        pacifico.setTypeface(facePacifico);

        TextView quicksand = (TextView) findViewById(R.id.txtFontQuicksand);
        Typeface faceQuicksand = Typeface.createFromAsset(getAssets(),
                "fonts/quicksank.otf");
        quicksand.setTypeface(faceQuicksand);

        TextView walkway = (TextView) findViewById(R.id.txtFontWalkway);
        Typeface faceWalkway = Typeface.createFromAsset(getAssets(),
                "fonts/walkway.ttf");
        walkway.setTypeface(faceWalkway);


        TextView sofia = (TextView) findViewById(R.id.txtFontSofia);
        Typeface faceSofia = Typeface.createFromAsset(getAssets(),
                "fonts/sofia.otf");
        sofia.setTypeface(faceSofia);

        TextView blackjack = (TextView) findViewById(R.id.txtFontBlackjack);
        Typeface faceBlackjack = Typeface.createFromAsset(getAssets(),
                "fonts/blackjack.otf");
        blackjack.setTypeface(faceBlackjack);


        SharedPreferences fontSettings=getSharedPreferences("fontPath",0);
        String firstFont=fontSettings.getString("path","deafult");

        final SharedPreferences.Editor editor=fontSettings.edit();

        btnDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putString("path","default");
                editor.commit();
                Toast.makeText(FontsActivity.this,"Font set : Default",Toast.LENGTH_SHORT).show();
                restart();

            }
        });

        btnDancing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putString("path","fonts/dacing.otf");
                editor.commit();
                Toast.makeText(FontsActivity.this,"Font set : Dancing Script",Toast.LENGTH_SHORT).show();
                restart();

            }
        });

        btnKaushan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putString("path","fonts/kaushan.otf");
                editor.commit();
                Toast.makeText(FontsActivity.this,"Font set : Kaushan Script",Toast.LENGTH_SHORT).show();
                restart();

            }
        });

        btnPacifico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putString("path","fonts/pacifico.ttf");
                editor.commit();
                Toast.makeText(FontsActivity.this,"Font set : Pacifico",Toast.LENGTH_SHORT).show();
                restart();

            }
        });

        btnWalkway.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putString("path","fonts/walkway.otf");
                Toast.makeText(FontsActivity.this,"Font set : Walkway",Toast.LENGTH_SHORT).show();
                restart();

            }
        });
        btnQuicksand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putString("path","fonts/quicksank.otf");
                editor.commit();
                Toast.makeText(FontsActivity.this,"Font set : Quicksand",Toast.LENGTH_SHORT).show();
                restart();

            }
        });

        btnSofia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putString("path","fonts/sofia.otf");
                editor.commit();
                Toast.makeText(FontsActivity.this,"Font set : Sofia",Toast.LENGTH_SHORT).show();
                restart();

            }
        });

        btnBlackjack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editor.putString("path","fonts/blackjack.otf");
                editor.commit();
                Toast.makeText(FontsActivity.this,"Font set : Black Jack",Toast.LENGTH_SHORT).show();
                restart();

            }
        });


    }

    public void restart(){

        AlarmManager alm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alm.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, PendingIntent.getActivity(this, 0, new Intent(this, com.sdskapps.circle.MainActivity.class), 0));

        Process.killProcess(Process.myPid());
    }

}
