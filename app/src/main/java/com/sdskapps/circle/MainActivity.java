package com.sdskapps.circle;

import android.Manifest;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import com.google.android.material.tabs.TabLayout;
import androidx.core.content.FileProvider;
import androidx.core.content.PermissionChecker;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.jaredrummler.cyanea.Cyanea;
import com.jaredrummler.cyanea.app.CyaneaAppCompatActivity;
import com.jaredrummler.cyanea.prefs.CyaneaSettingsActivity;

import org.polaric.colorful.CActivity;
import org.polaric.colorful.ColorPickerDialog;
import org.polaric.colorful.Colorful;

import java.io.File;

import androidx.viewpager.widget.ViewPager;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends CyaneaAppCompatActivity {
        //AppCompatActivity

    private FirebaseAuth mAuth;
    private Toolbar mToolbar;
    private DatabaseReference mUserRef,mGlobalNotificationRef;

    private TextView chatLabel,friendsLabel,requestsLabel,globalNotificationLabel;
long enq = 0;
    private ViewPager mViewPager;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private TabLayout mTabLayout;
    private SlidingTabLayout mSlidingTabLayout;
    private LinearLayout mLinearLayout;
    private ValueEventListener notificationListner;
    private int cPrimary= Colorful.ThemeColor.RED.getColorRes();
    private int color= Colorful.getThemeDelegate().getPrimaryColor().getColorRes();
    private int accent = Colorful.getThemeDelegate().getAccentColor().getColorRes();

//    private int cPrimary = Cyanea.getInstance().getPrimary();
//    private int color = Cyanea.getInstance().getPrimary();
//    private int accent = getCyanea().getAccent();
    private int DRAW_OVER_OTHER_APPS_TOKEN = 96;
    private Boolean doubleBackToExitPressedOnce = false;
    private String permissions[] = {Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE};

    private Colorful.ThemeColor colorNew = Colorful.ThemeColor.DEEP_ORANGE;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        SharedPreferences settings=getSharedPreferences("firstCircleRun",0);
        boolean firstRun=settings.getBoolean("firstRun",false);
        if(firstRun==false)//if running for first time
        {

            Intent i=new Intent(MainActivity.this,IntroActivity.class);//Activity to be     launched For the First time
            startActivity(i);
            finish();
        }
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();

     //   Toast.makeText(this, "Primary " + XcPrimary + "\nColor " + Xcolor + "\nAccent " + Xaccent, Toast.LENGTH_SHORT).show();
      /**  Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ).withListener(new MultiplePermissionsListener() {
            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {/* ... */
    //}
       //     @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {/* ... */}
     //   }).check();

        mToolbar=(Toolbar) findViewById(R.id.main_page_toolbar);
        mToolbar.setElevation(0);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle("Circle  ");
      //  mToolbar.setTitleTextColor();

      //  mLinearLayout.setBackgroundColor(getResources().getColor(color));


        chatLabel = (TextView) findViewById(R.id.chatLabel);
        friendsLabel = (TextView) findViewById(R.id.friendsLabel);
        requestsLabel = (TextView) findViewById(R.id.RequestsLabel);
        globalNotificationLabel = (TextView) findViewById(R.id.globalNotification);


        if(mAuth.getCurrentUser() != null) {

            checkPermission(getApplicationContext(),permissions);

            if(Build.VERSION.SDK_INT >= 23){
                if(!Settings.canDrawOverlays(this)){
                        showOverlayDialog();
                }
            }



                SharedPreferences changeLogs = getSharedPreferences("firstChangeRun", 0);
                String firstChange = changeLogs.getString("firstChange", "default");
                if (!firstChange.equals(getString(R.string.app_version))) {
                    SharedPreferences.Editor editor = changeLogs.edit();
                    editor.putString("firstChange", getString(R.string.app_version));
                    editor.commit();

                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Changelogs " + getString(R.string.app_version))
                            .setMessage(getResources().getString(R.string.changelogs))
                            .setNeutralButton("Ok", null)
                            .setCancelable(false)
                            .create();
                    alertDialog.show();
                }

            mGlobalNotificationRef = FirebaseDatabase.getInstance().getReference().child("globalNotification");
            mUserRef = FirebaseDatabase.getInstance().getReference().child("Users").child(mAuth.getCurrentUser().getUid());
          notificationListner = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String globalNotificationData = dataSnapshot.child("notificationValue").getValue().toString();
                    String update = dataSnapshot.child("Update").getValue().toString();
                    String link = dataSnapshot.child("link").getValue().toString();
                    String changes = dataSnapshot.child("changes").getValue().toString();

                    if(globalNotificationData.equals("default")){

                        globalNotificationLabel.setVisibility(View.GONE);

                    }
                    else {

                        globalNotificationLabel.setVisibility(View.VISIBLE);
                        globalNotificationLabel.setText(globalNotificationData);
                        Linkify.addLinks(globalNotificationLabel,Linkify.ALL);

                    }

                    if(!update.equals("default")){
                        if(!update.equals(getString(R.string.app_version))){
                            if(globalNotificationData.equals("default")) {
                                globalNotificationLabel.setVisibility(View.VISIBLE);
                                globalNotificationLabel.setText("Update Available \nDownload Now: " + link);
                                Linkify.addLinks(globalNotificationLabel, Linkify.ALL);
                            }
                            showDialog(update,link,changes);
                        }
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };

        }
        //Tabs
        mViewPager= (ViewPager)findViewById(R.id.main_tabPager);
        mViewPager.setOffscreenPageLimit(2);
       // mViewPager.bringToFront();

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager.setAdapter(mSectionsPagerAdapter);

        int[] colors = {color, accent};
        GradientDrawable gd = new GradientDrawable();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setCornerRadius(0f);
        gd.setOrientation(GradientDrawable.Orientation.TL_BR);
        gd.setColors(new int[]{
                getResources().getColor(color),
                getResources().getColor(color),
                getResources().getColor(accent),
                getResources().getColor(accent)
        });
       // mViewPager.setBackground(gd);



//        mTabLayout=(TabLayout)findViewById(R.id.main_tabs);
  //      mTabLayout.setupWithViewPager(mViewPager);

        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.main_tabs);
       mSlidingTabLayout.setViewPager(mViewPager);

        chatLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mViewPager.setCurrentItem(0);

            }
        });

        friendsLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mViewPager.setCurrentItem(1);

            }
        });

        requestsLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mViewPager.setCurrentItem(2);

            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                changetabs(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void changetabs(int position){

        if(position == 0){

            chatLabel.setTextColor(getResources().getColor(R.color.white));
            chatLabel.setTextSize(24);

            friendsLabel.setTextColor(getResources().getColor(R.color.lightWhite));
            friendsLabel.setTextSize(16);

            requestsLabel.setTextColor(getResources().getColor(R.color.lightWhite));
            requestsLabel.setTextSize(16);

        }

        if(position == 1){

            chatLabel.setTextColor(getResources().getColor(R.color.lightWhite));
            chatLabel.setTextSize(16);

            friendsLabel.setTextColor(getResources().getColor(R.color.white));
            friendsLabel.setTextSize(24);

            requestsLabel.setTextColor(getResources().getColor(R.color.lightWhite));
            requestsLabel.setTextSize(16);

        }

        if(position == 2){

            chatLabel.setTextColor(getResources().getColor(R.color.lightWhite));
            chatLabel.setTextSize(16);

            friendsLabel.setTextColor(getResources().getColor(R.color.lightWhite));
            friendsLabel.setTextSize(16);

            requestsLabel.setTextColor(getResources().getColor(R.color.white));
            requestsLabel.setTextSize(24);

        }

    }

    @Override
    public void onStart() {
        super.onStart();

        checkPermission(getApplicationContext(),permissions);

        FirebaseUser currentUser=mAuth.getCurrentUser();
        if(currentUser == null)
        {
            sendToStart();
        } else{
            mUserRef.child("online").setValue("true");
            mGlobalNotificationRef.addValueEventListener(notificationListner);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
      //  mGlobalNotificationRef.removeEventListener(notificationListner);
        FirebaseUser currentUser=mAuth.getCurrentUser();
        if(currentUser != null) {
            mUserRef.child("online").setValue(ServerValue.TIMESTAMP);

        } else{
//            mGlobalNotificationRef.removeEventListener(notificationListner);
        }
    }

    private void sendToStart() {
        Intent startIntent= new Intent(MainActivity.this,StartActivity.class);
        startIntent.putExtra("colorPrimary",cPrimary);
        startActivity(startIntent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if(item.getItemId()==R.id.main_logout_btn){
             FirebaseAuth.getInstance().signOut();
            sendToStart();
        }
        if(item.getItemId()==R.id.main_search){
            Intent searchIntent = new Intent(MainActivity.this,SearchActivity.class);
            startActivity(searchIntent);
        }

        if(item.getItemId()==R.id.main_settings_btn){

            Intent settingsIntent = new Intent(MainActivity.this, SettingsActivity.class);
            settingsIntent.putExtra("colorPrimary",cPrimary);
            startActivity(settingsIntent);

        }

        if(item.getItemId() == R.id.main_cyaneaThemes_btn){
            startActivity(new Intent(MainActivity.this, CyaneaSettingsActivity.class));
        }
        if(item.getItemId() == R.id.main_font){
            startActivity(new Intent(MainActivity.this,FontsActivity.class));
        }
        if(item.getItemId() == R.id.main_all_btn){
            Intent settingsIntent = new Intent(MainActivity.this, UsersActivity.class);
            settingsIntent.putExtra("colorPrimary",cPrimary);
            startActivity(settingsIntent);
        }
        if(item.getItemId() ==R.id.main_theme_btn){
            CharSequence options[]=new CharSequence[]{"Primary Color", "Accent Color"};
            AlertDialog.Builder builder= new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Select Option");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    //Click Events
                    if(i == 0){
                        //----------Colorful-----------------
                        ColorPickerDialog dialog = new ColorPickerDialog(MainActivity.this);
                        dialog.setTitle("Accent Color");
                        dialog.setOnColorSelectedListener(new ColorPickerDialog.OnColorSelectedListener() {
                            @Override
                            public void onColorSelected(Colorful.ThemeColor colorFul) {
                                //TODO: Do something with the color
                                colorNew=colorFul;
                                Colorful.config(MainActivity.this)
                                        .primaryColor(colorFul)
                                        .translucent(false)
                                        .dark(false)
                                        .apply();
                                recreate();
                                cPrimary= colorFul.getColorRes();
                            }
                        });
                        dialog.show();
                        //----------Colorful-----------------
                    }
                    if(i == 1){
                        //Chats
                        //----------Colorful-----------------
                        ColorPickerDialog dialog = new ColorPickerDialog(MainActivity.this);
                                dialog.setTitle("Accent Color");
                        dialog.setOnColorSelectedListener(new ColorPickerDialog.OnColorSelectedListener() {
                            @Override
                            public void onColorSelected(Colorful.ThemeColor colorFul) {
                                //TODO: Do something with the color
                                colorNew=colorFul;
                                Colorful.config(MainActivity.this)
                                        .accentColor(colorFul)
                                        .translucent(false)
                                        .dark(false)
                                        .apply();
                                recreate();
                                cPrimary= colorFul.getColorRes();
                            }
                        });
                        dialog.show();
                        //----------Colorful-----------------

                    }

                }
            });
            builder.show();





        }
        if(item.getItemId() == R.id.main_about_btn) {
            Intent settingsIntent = new Intent(MainActivity.this, AboutActivity.class);
            settingsIntent.putExtra("colorPrimary",cPrimary);
            startActivity(settingsIntent);
        }

        return true;
    }

    public void showDialog(String update, String link, String changes){

       /* DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        Uri uri = Uri.parse(link); // missing 'http://' will cause crashed
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                        dialog.dismiss();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        dialog.dismiss();
                        break;

                    default:

                }
            }
        }; */

        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView dialogTitle = (TextView) dialog.findViewById(R.id.customDialogTitle);
        TextView dialogBody = (TextView) dialog.findViewById(R.id.customDialogBody);
        dialogBody.setMovementMethod(new ScrollingMovementMethod());
        ImageView coloredHeader = (ImageView) dialog.findViewById(R.id.customDialogHeader);
        Button btnNegative = (Button) dialog.findViewById(R.id.customDialogBtnNegative);
        Button btnPositive = (Button) dialog.findViewById(R.id.customDialogBtnPostive);

        btnPositive.setText("Update");
        btnNegative.setText("Cancel");

        GradientDrawable gradientDrawable = (GradientDrawable) coloredHeader.getBackground();
        GradientDrawable btnDrawable = (GradientDrawable) btnPositive.getBackground();
        btnDrawable.setColor(getResources().getColor(color));
        gradientDrawable.setColor(getResources().getColor(color));
        // coloredHeader.setBackgroundColor(getResources().getColor(color));
        dialogTitle.setText("Update Available!");
        dialogBody.setText("A newer version (" + update + ") is available and ready to be downloaded. \nChangelogs\n" + changes);

        dialog.show();

        btnPositive.setOnClickListener( v -> {



            String dirPath = Environment.getExternalStorageDirectory() + "/" + "Circle" + "/" + "Circle Updates" + "/";
            File dir = new File(dirPath);
            if(!dir.exists()){
                dir.mkdirs();
            }

            String path = Environment.getExternalStorageDirectory() + "/" + "Circle" + "/" + "Circle Updates" + "/" + update + ".apk";
            File file = new File(path);

            if(btnPositive.getText().equals("INSTALL")){

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Uri apkUri = FileProvider.getUriForFile(MainActivity.this, BuildConfig.APPLICATION_ID + ".fileprovider", file);
                    Intent apkintent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                    apkintent.setData(apkUri);
                    apkintent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    MainActivity.this.startActivity(apkintent);
                } else {
                    Uri apkUri = Uri.fromFile(file);
                    Intent apkintent = new Intent(Intent.ACTION_VIEW);
                    apkintent.setDataAndType(apkUri, "application/vnd.android.package-archive");
                    apkintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    MainActivity.this.startActivity(apkintent);
                }
            }

            if(file.exists()){
                dialogBody.setText("Latest Update already downloaded, please install it using file manager.");
                btnPositive.setText("INSTALL");

            }else {
                btnPositive.setText("Update");
                DownloadManager downloadmanager = (DownloadManager) MainActivity.this.
                        getSystemService(Context.DOWNLOAD_SERVICE);
                Uri uri = Uri.parse(link);
                DownloadManager.Request request = new DownloadManager.Request(uri);

                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                //  request.setDestinationInExternalFilesDir(context, destinationDirectory, fileName + fileExtension);
                request.setDestinationInExternalPublicDir("/Circle/Circle Updates", update+".apk");

                enq = downloadmanager.enqueue(request);
                Toast.makeText(MainActivity.this, "Downloading...", Toast.LENGTH_SHORT).show();
               // registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
                dialog.dismiss();
            }
        });

        btnNegative.setOnClickListener( v -> {
            dialog.dismiss();
        });

    /*    final AlertDialog d = new AlertDialog.Builder(MainActivity.this)
                .setTitle("Update Available")
                .setMessage("A newer version (" + update + ") is available and ready to be downloaded. \nChangelogs\n" + changes)
                .setPositiveButton("Update Now", dialogClickListener)
                .setNegativeButton("Not Now",dialogClickListener)
                .setCancelable(false)
                .create();

        d.show();

        TextView t = (TextView) d.findViewById(android.R.id.message); */

        Linkify.addLinks(dialogBody,Linkify.ALL);
     //   ((TextView)d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());

    }

    public void showOverlayDialog(){

   /*     DialogInterface.OnClickListener dialogClick = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch(which){
                  case  DialogInterface.BUTTON_POSITIVE:
                      Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                      startActivityForResult(intent, DRAW_OVER_OTHER_APPS_TOKEN);
                      dialog.dismiss();
                      break;
                   case DialogInterface.BUTTON_NEGATIVE:
                       dialog.dismiss();
                       break;

                }
            }
        };
 */

        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView dialogTitle = (TextView) dialog.findViewById(R.id.customDialogTitle);
        TextView dialogBody = (TextView) dialog.findViewById(R.id.customDialogBody);
        dialogBody.setMovementMethod(new ScrollingMovementMethod());
        ImageView coloredHeader = (ImageView) dialog.findViewById(R.id.customDialogHeader);
        Button btnNegative = (Button) dialog.findViewById(R.id.customDialogBtnNegative);
        Button btnPositive = (Button) dialog.findViewById(R.id.customDialogBtnPostive);
        btnPositive.setText("Sure!");
        btnNegative.setText("No");

        GradientDrawable gradientDrawable = (GradientDrawable) coloredHeader.getBackground();
        GradientDrawable btnDrawable = (GradientDrawable) btnPositive.getBackground();
        btnDrawable.setColor(getResources().getColor(color));
        gradientDrawable.setColor(getResources().getColor(color));
       // coloredHeader.setBackgroundColor(getResources().getColor(color));
        dialogTitle.setText("PopUp Notifications");
        dialogBody.setText("To display PopUp notifications, you need to enable \'Draw Over Other Apps\' from settings, or you\'ll not receive PopUp notifications. " +
                "\nHowever, on some devices this may affect the performance of the device during some heavy tasks.\n" +
                "Do you want to enable the permission?");

        dialog.show();

        btnPositive.setOnClickListener( v -> {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, DRAW_OVER_OTHER_APPS_TOKEN);
            dialog.dismiss();
        });

        btnNegative.setOnClickListener( v -> {
            dialog.dismiss();
        });
  /*      final AlertDialog.Builder d = new AlertDialog.Builder(MainActivity.this);

        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        View dialogCustomView = inflater.inflate(R.layout.custom_dialog,null);
        d.setView(dialogCustomView);

        TextView dialogTitle = (TextView) dialogCustomView.findViewById(R.id.customDialogTitle);
        TextView dialogBody = (TextView) dialogCustomView.findViewById(R.id.customDialogBody);

        dialogTitle.setText("PopUp Notifications");
        dialogBody.setText("To display PopUp notifications, you need to enable \'Draw Over Other Apps\' from settings, or you\'ll not receive PopUp notifications. " +
                "\nHowever, on some devices this may affect the performance of the device during some heavy tasks.\n" +
                 "Do you want to enable the permission?");

        AlertDialog dialog = d.create();
        dialog.show(); */


          /*      .setTitle("Message Notifications")
                .setMessage("To display PopUp notifications, you need to enable \'Draw Over Other Apps\' from settings, or you\'ll not receive PopUp notifications. " +
                        "\nHowever, on some devices this may affect the performance of the device during some heavy tasks.\n" +
                        "Do you want to enable the permission?")
                .setPositiveButton("Sure!", dialogClick)
                .setNegativeButton("Not Now",dialogClick)
                .setNeutralButton("Never",dialogClick)
                .setCancelable(false)
                .create();
        d.show(); */

    }

    @Override
    protected void onResume() {
        super.onResume();

      //  mGlobalNotificationRef.addListenerForSingleValueEvent(notificationListner);
        checkPermission(getApplicationContext(),permissions);
        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mGlobalNotificationRef.removeEventListener(notificationListner);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
    }
    public boolean checkPermission(Context context, String... permission) {

        boolean nr = true;

        for (int i = 0; i < permission.length; i++) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                // targetSdkVersion >= Android M, we can
                // use Context#checkSelfPermission
                nr = context.checkSelfPermission(permission[i])
                        == PackageManager.PERMISSION_GRANTED;
            } else {
                // targetSdkVersion < Android M, we have to use PermissionChecker
                nr = PermissionChecker.checkSelfPermission(context, permission[i])
                        == PermissionChecker.PERMISSION_GRANTED;
            }

            if (!nr) {
               Intent permissionIntent = new Intent(MainActivity.this,PermissionActivity.class);
               permissionIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
               startActivity(permissionIntent);
            }
        }
        return nr;
    }
 /*   BroadcastReceiver onComplete=new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {

            String action = intent.getAction();
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(enq);
                DownloadManager downloadManager = (DownloadManager)getSystemService(DOWNLOAD_SERVICE);
                Cursor c = downloadManager.query(query);
                if (c.moveToFirst()) {
                    int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);
                    if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                        String uriString = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                        //TODO : Use this local uri and launch intent to open file
                        String title = c.getString(c.getColumnIndex(DownloadManager.COLUMN_TITLE));
                        Uri uri = Uri.parse(uriString);
                        String path = Environment.getExternalStorageDirectory() + "/" + "Circle" + "/" + "Circle Updates" + "/" + title;
                        File file = new File(path);

                      //  Uri u = FileProvider.getUriForFile(MainActivity.this,"com.sdskapps.circle.fileprovider",file);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            Uri apkUri = FileProvider.getUriForFile(MainActivity.this, BuildConfig.APPLICATION_ID + ".fileprovider", file);
                            Intent apkintent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                            intent.setData(uri);
                            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(apkintent);
                        } else {
                            Uri apkUri = Uri.fromFile(file);
                            Intent apkintent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(uri, "application/vnd.android.package-archive");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(apkintent);
                        }

                    }
                }
            }

            unregisterReceiver(onComplete);

        }
    };  */
}
