package com.sdskapps.circle;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sdskapps.circle.mail.MailActivity;

import org.polaric.colorful.CActivity;
import org.polaric.colorful.Colorful;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AboutActivity extends CActivity {

    private TextView loveText;
    private int fivePress = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        Circle circle = (Circle) findViewById(R.id.circle);
        CircleAngleAnimation animation = new CircleAngleAnimation(circle, 360);
        animation.setDuration(2500);
        circle.startAnimation(animation);
        Button b = (Button) findViewById(R.id.btnTest);

        loveText = (TextView) findViewById(R.id.txtLoveAbout);

        loveText.setOnClickListener( v -> {
            if (fivePress == 5) {
                fivePress = 0;
                Toast.makeText(this, "Shubhankar Kotnala", Toast.LENGTH_SHORT).show();
            }

            this.fivePress += 1;

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    fivePress=0;
                }
            }, 2000);
        });

        loveText.setOnLongClickListener((view) -> {
            startActivity(new Intent(this, DbFix.class));
            return true;
        });

        b.setOnClickListener( view -> {

        AchievementUnlocked achievementUnlocked = new AchievementUnlocked(getApplicationContext());

            achievementUnlocked.setRounded(false).setLarge(false).setTopAligned(true).setDismissible(true);

        AchievementData data = new AchievementData();
        data.setTitle("Hello World");
        data.setSubtitle("This is a test notification");
        data.setBackgroundColor(getResources().getColor(Colorful.getThemeDelegate().getPrimaryColor().getColorRes()));
        data.setIcon(getDrawable(R.mipmap.ic_launcher));
        data.setTextColor(Color.WHITE);

        achievementUnlocked.show(data);

        Intent in = new Intent(AboutActivity.this,ShowNameDialog.class);
            in.putExtra("phone","test");
            in.putExtra("uid","test");
     //   startActivity(in);

            startActivity(new Intent(AboutActivity.this, MailActivity.class));


        });

        b.setOnLongClickListener(v -> {
            startActivity(new Intent(AboutActivity.this,AppSettings.class));
            return true;
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
