package com.sdskapps.circle;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.ImagePickerSheetView;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jaredrummler.cyanea.Cyanea;
import com.jaredrummler.cyanea.app.CyaneaAppCompatActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiPopup;

import org.polaric.colorful.CActivity;
import org.polaric.colorful.Colorful;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChatActivity extends CyaneaAppCompatActivity {

    public String mChatUser;
    private Toolbar mChatToolbar;
    private DatabaseReference mRoofRef;
    private TextView mTitleView;
    private TextView mLastSeenView;
    private TextView mStatusView;
    private CircleImageView mProfileImage;
    private FirebaseAuth mAuth;
    private String mCurrentUserId;
    private int color=Colorful.getThemeDelegate().getPrimaryColor().getColorRes();
    private ImageButton mChatAddBtn;
    private ImageButton mChatSendBtn;
    CustomRequest jsObjRequest;

    private ValueEventListener seenListner;

    private ProgressBar mProgress;
    long enq = 0;



    //new
    private EmojiEditText mChatMEssageView;
    private ImageButton mEmojiBtn;
    private View rootView;
    Timer t ;
    //new

    //Storage reference
    private StorageReference mImageStorage;
  //  private EditText mChatMEssageView;
    private RecyclerView mMessagesList;
    private SwipeRefreshLayout mRefreshLayout;
    private List<Messages> messagesList = new ArrayList<>();
    private LinearLayoutManager mLinearLayout;
    private MessageAdapter mAdapter;
    private DatabaseReference mMessageDatabase;

    private int mCurrentPage = 1;
    private DatabaseReference mUserRef;
    private CircleImageView profileImage;
    private RelativeLayout mRelativeLayoutBar;
    private RelativeLayout custom_chat_bar;
    private BottomSheetLayout chatBottomSheet;

  //  private RelativeLayout mRelativeSendBtn;

    //new solution
    private int itemPos = 0;
    private String mLastKey = "";
    private String mPreKey = "";

    private RelativeLayout mRelativeLayout;
    //Progress bar
    private ProgressDialog progressDialog;

    private Boolean typingStarted = false;

    private String mChatUserToken = "";
    private String mCurrentName = "";

    private Uri cameraImageUri = null;

    //----------SOME CONTENTS THAT MAKE THE CODE MORE FRIENDLY :D ---------------
    private static final int TOTAL_ITEMS_TO_LOAD = 15;
    private static final int GALLERY_PICK = 1;
    private int REQUEST_LOAD_IMAGE = 678;
    private int REQUEST_IMAGE_CAPTURE = 789;
    private int AUDIO_PICK = 890;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chat);

        mChatToolbar=(Toolbar) findViewById(R.id.chat_app_bar);
        setSupportActionBar(mChatToolbar);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setTitle("");
        mRoofRef= FirebaseDatabase.getInstance().getReference();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
      //  mRoofRef.keepSynced(true);

        mAuth=FirebaseAuth.getInstance();
        mCurrentUserId = mAuth.getCurrentUser().getUid();
        mUserRef = FirebaseDatabase.getInstance().getReference().child("Users").child(mCurrentUserId);
        mChatUser=getIntent().getStringExtra("user_id");

        SharedPreferences chatUSerId=getSharedPreferences("chatDetails",0);
        SharedPreferences.Editor editor = chatUSerId.edit();
        editor.putString("currentUserId",mCurrentUserId);
        editor.putString("chatUserId",mChatUser);
        editor.apply();

        mImageStorage= FirebaseStorage.getInstance().getReference();
        final String userName=getIntent().getStringExtra("user_name");
        final String status = getIntent().getStringExtra("status");



        //getSupportActionBar().setTitle(userName);

        LayoutInflater inflater=(LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View action_bar_view= inflater.inflate(R.layout.chat_custom_bar, null);
        actionBar.setCustomView(action_bar_view);

        //-----------Clear Notification Panel----------

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        //------------Custom Actionbar Items-----------

        mTitleView=(TextView) findViewById(R.id.custom_bar_title);
        mLastSeenView=(TextView) findViewById(R.id.custom_bar_seen);
        mProfileImage=(CircleImageView) findViewById(R.id.custom_bar_image);
        mStatusView = (TextView) findViewById(R.id.chatUserStatus);

        mProgress = (ProgressBar) findViewById(R.id.progressUploading);
        mProgress.setVisibility(View.GONE);

        chatBottomSheet = (BottomSheetLayout) findViewById(R.id.chat_bottomSheet);
      //  chatBottomSheet.setPeekOnDismiss(true);
        mRelativeLayoutBar = (RelativeLayout) findViewById(R.id.linearLayout);

  //      mMessageView = (MessageView) findViewById(R.id.message_view);
//        mMessageView.setLeftBubbleColor(color);


        mChatAddBtn = (ImageButton)findViewById(R.id.chat_add_btn);
        mEmojiBtn = (ImageButton) findViewById(R.id.chat_emoji_btn);
        mChatSendBtn = (ImageButton) findViewById(R.id.chat_send_btn);
        mChatMEssageView = (EmojiEditText) findViewById(R.id.chat_message_view);
        mAdapter = new MessageAdapter(messagesList, this);
        mMessagesList = (RecyclerView) findViewById(R.id.messages_list);
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.message_swipe_layout);

        mRefreshLayout.setSoundEffectsEnabled(true);
        mRefreshLayout.setRefreshing(true);

        custom_chat_bar = (RelativeLayout) findViewById(R.id.custom_chat_bar);

        mRelativeLayout = (RelativeLayout) findViewById(R.id.chatContainerLayout);
        mRelativeLayout.bringToFront();

        GradientDrawable gd = new GradientDrawable();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setCornerRadius(0f);
        gd.setOrientation(GradientDrawable.Orientation.TL_BR);
        gd.setColors(new int[]{
                getResources().getColor(color),
                getResources().getColor(color),
                getResources().getColor(Colorful.getThemeDelegate().getAccentColor().getColorRes()),
                getResources().getColor(Colorful.getThemeDelegate().getAccentColor().getColorRes())
        });

        mRelativeLayout.setBackground(gd);

        mLinearLayout = new LinearLayoutManager(this);
        mMessagesList.setHasFixedSize(true);
        mMessagesList.setLayoutManager(mLinearLayout);
        //Root View for emoticons
        rootView = findViewById(R.id.chat_activity_layout);
        /** or use any of these to get Root View programatically
         *  findViewById(android.R.id.content);
         * getWindow().getDecorView().findViewById(android.R.id.content)
         */


        /**
         * Shared Content Handling
         */


        if(getIntent().getStringExtra("sharedimage") != null || getIntent().getStringExtra("sharedText") != null){
            Toast.makeText(ChatActivity.this,"Received something!",Toast.LENGTH_SHORT).show();
            if(getIntent().getStringExtra("sharedText") != null){
                mChatMEssageView.setText(getIntent().getStringExtra("sharedText"));
            } else{
                CropImage.activity(Uri.parse(getIntent().getStringExtra("sharedimage"))).start(this);
            }
        }
        t = new Timer();

        mMessagesList.setAdapter(mAdapter);
     //   profileImage= (CircleImageView) findViewById(R.id.message_profile_layout);

        final EmojiPopup emojiPopup = EmojiPopup.Builder.fromRootView(rootView).build(mChatMEssageView);

        mUserRef.child("online").setValue("true");

        mUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mCurrentName = dataSnapshot.child("name").getValue().toString();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //---------------------TYPING STATUS------------------------------
        mChatMEssageView.addTextChangedListener(new TextWatcher() {



            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            public void afterTextChanged(Editable s) {

                if (!TextUtils.isEmpty(s.toString()) && s.toString().trim().length() == 1) {

                    //Log.i(TAG, “typing started event…”);

                    typingStarted = true;
                    mUserRef.child("online").setValue("online" + mChatUser);

                    //send typing started status

                } else if (s.toString().trim().length() == 0 && typingStarted) {

                    //Log.i(TAG, “typing stopped event…”);

                    typingStarted = false;
                    mUserRef.child("online").setValue("true");

                    //send typing stopped status

                }

            }

        });

        //---------------------TYPING STATUS------------------------------

        loadMessages();


        mTitleView.setText(userName);
        if(!status.trim().isEmpty()){
            mStatusView.setText(status);
            mStatusView.setSelected(true);
            mStatusView.setVisibility(View.VISIBLE);
        } else{
            mStatusView.setVisibility(View.GONE);
        }

        GradientDrawable bgShape = (GradientDrawable)mRelativeLayoutBar.getBackground();
        GradientDrawable bgSend = (GradientDrawable)mChatSendBtn.getBackground();
        bgShape.setAlpha(85);
        bgShape.setColor(ContextCompat.getColor(getApplicationContext(),color));
        bgSend.setColor(ContextCompat.getColor(getApplicationContext(),color));


        custom_chat_bar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent chatIntent = new Intent(ChatActivity.this, user_profile_view.class);
                chatIntent.putExtra("user_name",userName);
                chatIntent.putExtra("user_id",mChatUser);
                startActivity(chatIntent);
            }
        });


        mRoofRef.child("Users").child(mChatUser).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String online = dataSnapshot.child("online").getValue().toString();
                final String image= dataSnapshot.child("image").getValue().toString();
                mChatUserToken = dataSnapshot.child("device_token").getValue().toString();
                String status = dataSnapshot.child("status").getValue().toString();

                if(!status.trim().isEmpty()){
                    mStatusView.setText(status);
                    mStatusView.setVisibility(View.VISIBLE);
                }else{
                    mStatusView.setVisibility(View.GONE);
                }
                if(online.equals("true")){

                    mLastSeenView.setText("Online");

                }else if(online.equals("online" + mChatUser)){

                    mLastSeenView.setText("typing...");

                }
                else {
                    try{
                        GetTimeAgo getTimeAgo = new GetTimeAgo();
                        Long lastTime = Long.parseLong(online);
                        String lastSeenTime =getTimeAgo.getTimeAgo(lastTime,getApplicationContext());
                        mLastSeenView.setText(lastSeenTime);
                    }catch (NumberFormatException e){
                        mLastSeenView.setText("online");
                    }catch (Exception e){
                        mLastSeenView.setText(" ");
                        Log.d("onlineStatus",e.getMessage());
                    }

                }

                if(!image.equals("default")){

                    Picasso.with(ChatActivity.this).load(image).networkPolicy(NetworkPolicy.OFFLINE)
                            .placeholder(R.drawable.default_avatar1).into(mProfileImage, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                            Picasso.with(ChatActivity.this).load(image).placeholder(R.drawable.default_avatar1).into(mProfileImage);
                        }
                    });
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mRoofRef.child("Chat").child(mCurrentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(!dataSnapshot.hasChild(mChatUser)){

                    Map chatAddMap = new HashMap();
                    chatAddMap.put("seen",false);
                    chatAddMap.put("timestamp", ServerValue.TIMESTAMP);

                    Map chatUserMap= new HashMap();
                    chatUserMap.put("Chat/" + mCurrentUserId + "/" + mChatUser, chatAddMap);
                    chatUserMap.put("Chat/" + mChatUser + "/" + mCurrentUserId, chatAddMap);

                    mRoofRef.updateChildren(chatUserMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            if(databaseError != null){
                                Log.d("CHAT_LOG",databaseError.getMessage().toString());
                            }
                        }
                    });

                }



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        mChatSendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                
                sendMessage();
                
            }
        });

        mChatAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

          /*      Intent galleryIntent = new Intent();
                galleryIntent.setType("image/*");
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(galleryIntent, "Select Image"),GALLERY_PICK);
*/
              /*  CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(ChatActivity.this); */

              showSheetView();

           //---------------FLIPBOARD BOTTOM SHEET------------------


            }
        });

        mChatAddBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent galleryIntent = new Intent();
                galleryIntent.setType("image/*");
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

            //    startActivityForResult(Intent.createChooser(galleryIntent, "Select Image"), 21);
               // return false;
              //  showSheetView();
                return false;
            }
        });

        mEmojiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                  emojiPopup.toggle();

               // emojiPopup.toggle(); // Toggles visibility of the Popup.
               // emojiPopup.dismiss(); // Dismisses the Popup.
               // emojiPopup.isShowing(); // Returns true when Popup is showing.
            }
        });
        mChatMEssageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emojiPopup.dismiss();
            }
        });


        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

            mCurrentPage++;
                itemPos = 0;
               // loadMoreMessages();
                loadMessages();

            }
        });

    }

    private void showSheetView() {
        ImagePickerSheetView sheetView = new ImagePickerSheetView.Builder(this)
                .setMaxItems(30)
                .setShowCameraOption(createCameraIntent() != null)
                .setShowPickerOption(createPickIntent() != null)
                .setImageProvider(new ImagePickerSheetView.ImageProvider() {
                    @Override
                    public void onProvideImage(ImageView imageView, Uri imageUri, int size) {
                        Glide.with(ChatActivity.this)
                                .load(imageUri)
                                .centerCrop()
                                .crossFade()
                                .into(imageView);
                    }
                })
                .setOnTileSelectedListener(new ImagePickerSheetView.OnTileSelectedListener() {
                    @Override
                    public void onTileSelected(ImagePickerSheetView.ImagePickerTile selectedTile) {
                        chatBottomSheet.dismissSheet();
                        if (selectedTile.isCameraTile()) {
                            dispatchTakePictureIntent();
                        } else if (selectedTile.isPickerTile()) {
                            startActivityForResult(createPickIntent(), REQUEST_LOAD_IMAGE);
                        } else if (selectedTile.isImageTile()) {
                           // showSelectedImage(selectedTile.getImageUri());
                            CropImage.activity(selectedTile.getImageUri()).start(ChatActivity.this);
                        } else {
                            Toast.makeText(ChatActivity.this, "Something Went Wrong!", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setTitle("Select an image...")
                .setMaxItems(50)
                .create();

        chatBottomSheet.showWithSheetView(sheetView);
    }

    /**
     * For images captured from the camera, we need to create a File first to tell the camera
     * where to store the image.
     *
     * @return the File created for the image to be store under.
     */
    @Nullable
    private Intent createPickIntent() {
        Intent picImageIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (picImageIntent.resolveActivity(getPackageManager()) != null) {
            return picImageIntent;
        } else {
            return null;
        }
    }

    /**
     * This checks to see if there is a suitable activity to handle the {@link MediaStore#ACTION_IMAGE_CAPTURE}
     * intent and returns it if found. {@link MediaStore#ACTION_IMAGE_CAPTURE} is for letting another app take
     * a picture from the camera and store it in a file that we specify.
     *
     * @return A prepared intent if found.
     */
    @Nullable
    private Intent createCameraIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            return takePictureIntent;
        } else {
            return null;
        }
    }

    /**
     * This utility function combines the camera intent creation and image file creation, and
     * ultimately fires the intent.
     *
     * @see {@link #createCameraIntent()}
     * @see {@link #createImageFile()}
     */
   // @Override
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = createCameraIntent();
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent != null) {
            // Create the File where the photo should go
            try {
                File imageFile = createImageFile();
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            } catch (IOException e) {
                // Error occurred while creating the File
             //   genericError("Could not create imageFile for camera");
                Toast.makeText(ChatActivity.this, "Could not create imageFile for camera", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "CIRCLE_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File imageFile = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        cameraImageUri = Uri.fromFile(imageFile);
        return imageFile;
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.chat_menu,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if(item.getItemId() == R.id.menu_text_recog){

            Intent imageIntent = new Intent();
            imageIntent.setType("image/*");
            imageIntent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(imageIntent, "Select Image"),21);

        }
        if(item.getItemId() == R.id.menu_attach){
            String[] mimeTypes =
                    {"application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                            "application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                            "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                            "text/plain",
                            "application/pdf",
                            "application/zip",
                            "video/*",
                            "audio/*"};
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            Intent chooser = Intent.createChooser(intent,"Select File");
            startActivityForResult(chooser,98);
        }
        if(item.getItemId() == R.id.menu_audio){
            Intent audioIntent = new Intent();
            audioIntent.setType("audio/*");
            audioIntent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(audioIntent,AUDIO_PICK);
        }
        if(item.getItemId() == android.R.id.home){
            if(chatBottomSheet.isSheetShowing()){
                chatBottomSheet.dismissSheet();
            }else{
               super.onBackPressed();
            }
        }
        return true;
    }
        @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                CropImage.activity(cameraImageUri).start(this);
            }
            if (data != null) {


    /*    if(requestCode == 21){
            Uri selectedImageUri = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            runTextRecognition(bitmap);
        }  */

            if (requestCode == REQUEST_LOAD_IMAGE && data != null) {
                CropImage.activity(data.getData()).start(this);
            }


            if (requestCode == AUDIO_PICK) {
                Uri uri = data.getData();
              //  Toast.makeText(this, "File Name : " + getFileName(uri), Toast.LENGTH_SHORT).show();
            }

            if (requestCode == 98) {
                Uri uri = data.getData();

                String fileName = getFileName(uri);

                Log.d("Document :", fileName);

                String ext = fileName.substring(fileName.lastIndexOf('.'));
               // Toast.makeText(ChatActivity.this, ext, Toast.LENGTH_LONG).show();

                DatabaseReference user_message_push = mRoofRef.child("messages")
                        .child(mCurrentUserId).child(mChatUser).push();

                final String push_id = user_message_push.getKey();
                StorageReference filepath = mImageStorage.child("message_documents").child(push_id + ext);
                final String current_user_ref = "messages/" + mCurrentUserId + "/" + mChatUser;
                final String chat_user_ref = "messages/" + mChatUser + "/" + mCurrentUserId;
                UploadTask uploadTask = filepath.putFile(uri);
                // UploadTask uploadTask = filepath.putBytes(image_byte);
                String finalFileName = fileName;
                mProgress.setProgress(0);
                mProgress.setIndeterminate(true);
                mProgress.setVisibility(View.VISIBLE);
                uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        mProgress.setIndeterminate(false);
                        double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        //sets and increments value of progressbar
                        //progressDialog.incrementProgressBy((int) progress);
                        mProgress.incrementProgressBy((int) progress);
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // progressDialog.dismiss();
                        Map messageMap = new HashMap();

                        //String download_url = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
                        filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                String download_url = uri.toString();
                                 messageMap.put("message", download_url);
                                messageMap.put("seen", false);
                                messageMap.put("type", "document~" + finalFileName);
                                messageMap.put("time", ServerValue.TIMESTAMP);
                                messageMap.put("from", mCurrentUserId);

                                Map messageUserMap = new HashMap();
                                messageUserMap.put(current_user_ref + "/" + push_id, messageMap);
                                messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);

                                //    mChatMEssageView.setText("");

                                mProgress.setVisibility(View.GONE);

                        mRoofRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                if (databaseError != null) {
                                    Log.d("CHAT_LOG", databaseError.getMessage().toString());
                                }
                            }
                        });

                        Map chatAddMap = new HashMap();
                        chatAddMap.put("seen", false);
                        chatAddMap.put("timestamp", ServerValue.TIMESTAMP);

                        Map chatUserMap = new HashMap();
                        chatUserMap.put("Chat/" + mCurrentUserId + "/" + mChatUser, chatAddMap);
                        chatUserMap.put("Chat/" + mChatUser + "/" + mCurrentUserId, chatAddMap);

                        mRoofRef.updateChildren(chatUserMap, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                if (databaseError != null) {
                                    Log.d("CHAT_LOG", databaseError.getMessage().toString());
                                    Toast.makeText(ChatActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                                } else {
                                    //sendMessageNotification(mCurrentName,download_url,mChatUserToken,"message");
                                }
                            }
                        });
                            }
                        });




                    }
                });

            }

            if (requestCode == GALLERY_PICK && resultCode == RESULT_OK) {
                Uri imageUri = data.getData();

                CropImage.activity(imageUri).start(this);


            }
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();

                    File thumb_filePath = new File(resultUri.getPath());
                    final String current_user_ref = "messages/" + mCurrentUserId + "/" + mChatUser;
                    final String chat_user_ref = "messages/" + mChatUser + "/" + mCurrentUserId;

                    //----------IMAGE COMPRESSION---------------

                    Bitmap compressedImage = null;

                    try {

                        compressedImage = new Compressor(this)
                                .setMaxWidth(640)
                                .setMaxHeight(480)
                                .setQuality(50)
                                .compressToBitmap(thumb_filePath);
                        Log.d("ImageCompression", "Success");

                    } catch (java.io.IOException e) {
                        Log.d("ImageCompression", "Failure");
                    }

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    //TODO: check if it worls or not
                    compressedImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    final byte[] image_byte = baos.toByteArray();
                    //----------IMAGE COMPRESSION---------------

                    DatabaseReference user_message_push = mRoofRef.child("messages")
                            .child(mCurrentUserId).child(mChatUser).push();

                    final String push_id = user_message_push.getKey();
                    StorageReference filepath = mImageStorage.child("message_images").child(push_id + ".jpg");
                    //progress dialog
             /*   progressDialog = new ProgressDialog(this);
                progressDialog.setMax(100);
                progressDialog.setMessage("Uploading...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progressDialog.show();
                progressDialog.setCancelable(false);  */
                    mProgress.setVisibility(View.VISIBLE);
                    mProgress.setIndeterminate(true);
                    mProgress.setProgress(0);
                    Toast.makeText(this, "Uploading File", Toast.LENGTH_SHORT).show();
                    UploadTask uploadTask = filepath.putBytes(image_byte);
                    // UploadTask uploadTask = filepath.putBytes(image_byte);
                    uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            mProgress.setIndeterminate(false);
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            //sets and increments value of progressbar
                            //progressDialog.incrementProgressBy((int) progress);
                            mProgress.incrementProgressBy((int) progress);
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            // progressDialog.dismiss();
                            Map messageMap = new HashMap();

                           // String download_url = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
                            filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {

                                    String download_url = uri.toString();

                                    messageMap.put("message", download_url);
                                    messageMap.put("seen", false);
                                    messageMap.put("type", "image");
                                    messageMap.put("time", ServerValue.TIMESTAMP);
                                    messageMap.put("from", mCurrentUserId);

                                    Map messageUserMap = new HashMap();
                                    messageUserMap.put(current_user_ref + "/" + push_id, messageMap);
                                    messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);

                                    //    mChatMEssageView.setText("");

                                    mProgress.setVisibility(View.GONE);

                            mRoofRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    if (databaseError != null) {
                                        Log.d("CHAT_LOG", databaseError.getMessage().toString());
                                    }
                                }
                            });

                            Map chatAddMap = new HashMap();
                            chatAddMap.put("seen", false);
                            chatAddMap.put("timestamp", ServerValue.TIMESTAMP);

                            Map chatUserMap = new HashMap();
                            chatUserMap.put("Chat/" + mCurrentUserId + "/" + mChatUser, chatAddMap);
                            chatUserMap.put("Chat/" + mChatUser + "/" + mCurrentUserId, chatAddMap);

                            mRoofRef.updateChildren(chatUserMap, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    if (databaseError != null) {
                                        Log.d("CHAT_LOG", databaseError.getMessage().toString());
                                    } else {
                                        //sendMessageNotification(mCurrentName,download_url,mChatUserToken,"message");
                                    }
                                }
                            });
                                }
                            })  ;
                            // String download_url = filepath.getDownloadUrl().toString();
                            //Toast.makeText(ChatActivity.this, download_url, Toast.LENGTH_SHORT).show();

                        }
                    });
               /*         .addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                        if(task.isSuccessful()){

                            progressDialog.dismiss();

                            String download_url = task.getResult().getMetadata().getReference().getDownloadUrl().toString();
                            Map messageMap = new HashMap();
                            messageMap.put("message",download_url);
                            messageMap.put("seen", false);
                            messageMap.put("type", "image");
                            messageMap.put("time", ServerValue.TIMESTAMP);
                            messageMap.put("from", mCurrentUserId);

                            Map messageUserMap = new HashMap();
                            messageUserMap.put(current_user_ref + "/" + push_id, messageMap);
                            messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);

                            mChatMEssageView.setText("");

                            mRoofRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    if(databaseError != null){
                                        Log.d("CHAT_LOG",databaseError.getMessage().toString());
                                    }
                                }
                            });

                            Map chatAddMap = new HashMap();
                            chatAddMap.put("seen",false);
                            chatAddMap.put("timestamp", ServerValue.TIMESTAMP);

                            Map chatUserMap= new HashMap();
                            chatUserMap.put("Chat/" + mCurrentUserId + "/" + mChatUser, chatAddMap);
                            chatUserMap.put("Chat/" + mChatUser + "/" + mCurrentUserId, chatAddMap);

                            mRoofRef.updateChildren(chatUserMap, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                    if(databaseError != null){
                                        Log.d("CHAT_LOG",databaseError.getMessage().toString());
                                    } else {
                                        sendMessageNotification(mCurrentName,download_url,mChatUserToken,"message");
                                    }
                                }
                            });

                        }

                    }
                });   */

                }
            }
        }
    }

    private void loadMoreMessages(){


        final DatabaseReference messageRef = mRoofRef.child("messages").child(mCurrentUserId).child(mChatUser);
        Query messageQuery = messageRef.limitToLast(mCurrentPage * TOTAL_ITEMS_TO_LOAD);

        messageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {


                String cUser = mChatUser;

                Log.d("Chat user",cUser);
                Log.d("DB User",dataSnapshot.child("from").getValue().toString());

                String dbUser = dataSnapshot.child("from").getValue().toString();

          /*      if(cUser.equals(dbUser)) {
                    String seen = dataSnapshot.child("seen").getValue().toString();
                   // Toast.makeText(ChatActivity.this,seen,Toast.LENGTH_SHORT).show();
                    if (seen.equals("false")) {
                        //   dataSnapshot.child("from"
                        String key = dataSnapshot.getKey();
                        messageRef.child(key).child("seen").setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                            Log.d("Seen More","true");
                            }
                        });

                        mRoofRef.child("messages").child(mChatUser).child(mCurrentUserId)
                                .child(key).child("seen")
                                .setValue(true).addOnCompleteListener(task -> Log.d("Seen More","true"));

                    }
                }  */

                Messages message = dataSnapshot.getValue(Messages.class);

                String messageKey = dataSnapshot.getKey();

                if(!mPreKey.equals(messageKey)){
                    messagesList.add(itemPos++,message);


                } else {
                    mPreKey = mLastKey;
                }


                if(itemPos == 1){

                    mLastKey = messageKey;

                }



                Log.d("TOTALKEYS", "Last key :" + mLastKey +"| PREV Key :" + mPreKey + "| Message Key" + messageKey);

                mAdapter.notifyDataSetChanged();


                mRefreshLayout.setRefreshing(false);

                mLinearLayout.scrollToPositionWithOffset(15,0);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

               Messages newMessage = dataSnapshot.getValue(Messages.class);
                if(newMessage != null) {

                    for(int i=0; i<messagesList.size();i++){
                        if(messagesList.get(i).getTime() == newMessage.getTime()){
                            messagesList.remove(i);
                          //  messagesList.notify();
                            mAdapter.notifyItemRemoved(i);
                            messagesList.add(i,newMessage);
                            mAdapter.notifyItemInserted(i);
                            mAdapter.notifyDataSetChanged();
                            Log.d("LoadMoreMessage","onChange()");
                            break;
                        }
                    }

                }
                mAdapter.notifyDataSetChanged();


            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void loadMessages() {

        final DatabaseReference messageRef = mRoofRef.child("messages").child(mCurrentUserId).child(mChatUser);
        Query messageQuery = messageRef.limitToLast(mCurrentPage * TOTAL_ITEMS_TO_LOAD);
       // Query messageQuery = messageRef.limitToLast(TOTAL_ITEMS_TO_LOAD);
        messageQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot data) {
                String cUser = mChatUser;
                messagesList.clear();

                if (data.exists()) {



                for (DataSnapshot dataSnapshot : data.getChildren()) {
                    try{

                    String dbUser = dataSnapshot.child("from").getValue().toString();

                    if (cUser.equals(dbUser)) {

                        //   dataSnapshot.child("from"
                        String seen = dataSnapshot.child("seen").getValue().toString();
                        // Toast.makeText(ChatActivity.this, seen, Toast.LENGTH_SHORT).show();
                        if (seen.equals("false")) {
                            String key = dataSnapshot.getKey();
                            messageRef.child(key).child("seen").setValue(true).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    // Toast.makeText(ChatActivity.this, "SEEN", Toast.LENGTH_SHORT).show();
                                }
                            });
                            mRoofRef.child("messages").child(mChatUser).child(mCurrentUserId)
                                    .child(key).child("seen")
                                    .setValue(true).addOnCompleteListener(task -> Log.d("Seen", "True"));

                        }
                    }


                    Messages message = dataSnapshot.getValue(Messages.class);
                    message.setKey(dataSnapshot.getKey());


                    itemPos++;

                    if (itemPos == 1) {

                        String messageKey = dataSnapshot.getKey();

                        mLastKey = messageKey;
                        mPreKey = messageKey;

                    }


                    messagesList.add(message);
                    mAdapter.notifyDataSetChanged();
                    }catch(Exception e){
                        dataSnapshot.getRef().removeValue();
                    }
                }
            }



                if(mCurrentPage != 1){
                  mLinearLayout.scrollToPositionWithOffset(15,0);
                }else {
                    mMessagesList.scrollToPosition(messagesList.size() - 1);
                }
//                mMessagesList.smoothScrollToPosition(messagesList.size()-1);
                mRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

     /*   messageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//
                String cUser = mChatUser;

                String dbUser = dataSnapshot.child("from").getValue().toString();

                if(cUser.equals(dbUser)){

                    //   dataSnapshot.child("from"
                   String seen = dataSnapshot.child("seen").getValue().toString();
                   // Toast.makeText(ChatActivity.this, seen, Toast.LENGTH_SHORT).show();
                   if(seen.equals("false")) {
                       String key = dataSnapshot.getKey();
                       messageRef.child(key).child("seen").setValue(true).addOnSuccessListener(new OnSuccessListener<Void>() {
                           @Override
                           public void onSuccess(Void aVoid) {
                              // Toast.makeText(ChatActivity.this, "SEEN", Toast.LENGTH_SHORT).show();
                           }
                       });
                       mRoofRef.child("messages").child(mChatUser).child(mCurrentUserId)
                               .child(key).child("seen")
                               .setValue(true).addOnCompleteListener(task -> Log.d("Seen","True"));

                   }
                }


                Messages message = dataSnapshot.getValue(Messages.class);


                itemPos++;

                if(itemPos == 1){

                    String messageKey = dataSnapshot.getKey();

                    mLastKey = messageKey;
                    mPreKey = messageKey;

                }


                messagesList.add(message);
                mAdapter.notifyDataSetChanged();


              //  mMessagesList.scrollToPosition(messagesList.size() - 1);
                mMessagesList.smoothScrollToPosition(messagesList.size()-1);
                mRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
               Messages newMessage = dataSnapshot.getValue(Messages.class);
                if(newMessage != null) {

                    for(int i=0; i<messagesList.size();i++){
                        if(messagesList.get(i).getTime() == newMessage.getTime()){
                            messagesList.remove(i);
                         //   messagesList.notify();
                            mAdapter.notifyItemRemoved(i);
                            messagesList.add(i,newMessage);
//                            messagesList.notify();
                            mAdapter.notifyItemInserted(i);
                            mAdapter.notifyDataSetChanged();
                            Log.d("LoadMessage","onChange()");
                            break;
                        }
                    }

                }
                mAdapter.notifyDataSetChanged();

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
*/
    }

    private void sendMessage() {

        String message = mChatMEssageView.getText().toString().trim();


        if(!TextUtils.isEmpty(message)){

            String current_user_ref = "messages/" + mCurrentUserId + "/" + mChatUser;
            String chat_user_ref = "messages/" + mChatUser + "/" + mCurrentUserId;

            DatabaseReference user_message_push=mRoofRef.child("messages").child(mCurrentUserId).child(mChatUser).push();

            String push_id=user_message_push.getKey();

            Map messageMap = new HashMap();
            messageMap.put("message",message);
            messageMap.put("seen", false);
            messageMap.put("type", "text");
            messageMap.put("time", ServerValue.TIMESTAMP);
            messageMap.put("from", mCurrentUserId);

            Map messageUserMap = new HashMap();
            messageUserMap.put(current_user_ref + "/" + push_id, messageMap);
            messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);

            mChatMEssageView.setText("");
            mRoofRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if(databaseError != null){
                        Log.d("CHAT_LOG",databaseError.getMessage().toString());
                    }
                }
            });


            Map chatAddMap = new HashMap();
            chatAddMap.put("seen",false);
            chatAddMap.put("timestamp", ServerValue.TIMESTAMP);

            Map chatUserMap= new HashMap();
            chatUserMap.put("Chat/" + mCurrentUserId + "/" + mChatUser, chatAddMap);
            chatUserMap.put("Chat/" + mChatUser + "/" + mCurrentUserId, chatAddMap);

            mRoofRef.updateChildren(chatUserMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if(databaseError != null){
                        Log.d("CHAT_LOG",databaseError.getMessage().toString());
                    } else {
                        //sendMessageNotification(mCurrentName,message,mChatUserToken,"message");
                    }
                }
            });

        }

        mChatMEssageView.setText("");

    }

    public String getFileName(Uri uri){
        String fileName = null;
        if ("file".equalsIgnoreCase(uri.getScheme()))
        {
            File file = new File(uri.getPath());
            fileName = file.getName();
        }
        else
        {
            String test = getPathFromUri(ChatActivity.this,uri);
            fileName = test.substring(test.lastIndexOf('/') + 1);
        }
        return fileName;
    }

   @Override
    public void onStart() {

        super.onStart();

      //  messageQuery.addValueEventListener(seenListner);

       NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
       notificationManager.cancelAll();
     t = new Timer();
//Set the schedule function and rate
        t.scheduleAtFixedRate(new TimerTask() {

                                  @Override
                                  public void run() {

                                    //  mUserRef.child("online").setValue("true");
                                      mRoofRef.child("Users").child(mChatUser).addListenerForSingleValueEvent(new ValueEventListener() {
                                          @Override
                                          public void onDataChange(DataSnapshot data) {
                                              String online = data.child("online").getValue().toString();
                                              final String image= data.child("image").getValue().toString();
                                              String status = data.child("status").getValue().toString();

                                              if(!status.trim().isEmpty()){
                                                  mStatusView.setText(status);
                                                  mStatusView.setVisibility(View.VISIBLE);
                                              }else{
                                                  mStatusView.setVisibility(View.GONE);
                                              }
                                              if(online.equals("true")){

                                                  mLastSeenView.setText("online");

                                              }else if(online.equals("online" + mCurrentUserId)){

                                                  mLastSeenView.setText("typing...");

                                              } else if(online.startsWith("online")){
                                                  mLastSeenView.setText("online");
                                              }
                                              else  {
                                                  try{
                                                      GetTimeAgo getTimeAgo = new GetTimeAgo();
                                                      Long lastTime = Long.parseLong(online);
                                                      String lastSeenTime =getTimeAgo.getTimeAgo(lastTime,getApplicationContext());
                                                      mLastSeenView.setText(lastSeenTime);
                                                  }catch (NumberFormatException e){
                                                      mLastSeenView.setText("online");
                                                  }catch (Exception e){
                                                      mLastSeenView.setText(" ");
                                                      Log.d("onlineStatus",e.getMessage());
                                                  }

                                              }



                                          }

                                          @Override
                                          public void onCancelled(DatabaseError databaseError) {

                                          }
                                      });//Called each time when 1000 milliseconds (1 second) (the period parameter)
                                  }

                              },
//Set how long before to start calling the TimerTask (in milliseconds)
                0,
//Set the amount of time between each execution (in milliseconds)
                500);

    }
    @Override
    protected void onStop() {
        super.onStop();
     //   messageQuery.removeEventListener(seenListner);
        SharedPreferences chatUSerId=getSharedPreferences("chatDetails",0);
        SharedPreferences.Editor editor = chatUSerId.edit();
        editor.putString("chatUserId","default");
        editor.commit();
     //mUserRef.child("online").setValue(ServerValue.TIMESTAMP);
        t.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();
     //   messageQuery.addValueEventListener(seenListner);
        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
        SharedPreferences chatUSerId=getSharedPreferences("chatDetails",0);
        SharedPreferences.Editor editor = chatUSerId.edit();
        editor.putString("chatUserId",mChatUser);
        editor.commit();
        t = new Timer();
//Set the schedule function and rate
        t.scheduleAtFixedRate(new TimerTask() {

                                  @Override
                                  public void run() {

                                   //   mUserRef.child("online").setValue("true");
                                      mRoofRef.child("Users").child(mChatUser).addListenerForSingleValueEvent(new ValueEventListener() {
                                          @Override
                                          public void onDataChange(DataSnapshot data) {
                                              String online = data.child("online").getValue().toString();
                                              final String image= data.child("image").getValue().toString();
                                              String status = data.child("status").getValue().toString();

                                              if(!status.trim().isEmpty()){
                                                  mStatusView.setText(status);
                                                  mStatusView.setVisibility(View.VISIBLE);
                                              }else{
                                                  mStatusView.setVisibility(View.GONE);
                                              }

                                              if(online.equals("true")){

                                                  mLastSeenView.setText("online");

                                              }else if(online.equals("online" + mCurrentUserId)){

                                                  mLastSeenView.setText("typing...");

                                              }else if(online.startsWith("online")){
                                                  mLastSeenView.setText("online");
                                              }
                                              else {
                                                  try{
                                                      GetTimeAgo getTimeAgo = new GetTimeAgo();
                                                      Long lastTime = Long.parseLong(online);
                                                      String lastSeenTime =getTimeAgo.getTimeAgo(lastTime,getApplicationContext());
                                                      mLastSeenView.setText(lastSeenTime);
                                                  }catch (NumberFormatException e){
                                                      mLastSeenView.setText("online");
                                                  }catch (Exception e){
                                                      mLastSeenView.setText(" ");
                                                  }

                                              }

                                          }

                                          @Override
                                          public void onCancelled(DatabaseError databaseError) {

                                          }
                                      });//Called each time when 1000 milliseconds (1 second) (the period parameter)
                                  }

                              },
//Set how long before to start calling the TimerTask (in milliseconds)
                0,
//Set the amount of time between each execution (in milliseconds)
                500);

    }

    @Override
    protected void onPause() {
        super.onPause();
     //   messageQuery.removeEventListener(seenListner);
        SharedPreferences chatUSerId=getSharedPreferences("chatDetails",0);
        SharedPreferences.Editor editor = chatUSerId.edit();
        editor.putString("chatUserId","default");
        editor.commit();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /*
    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(ChatActivity.this, android.R.anim.fade_in);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }  */


  /*  private void runTextRecognition(Bitmap mSelectedImage) {
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(mSelectedImage);
        FirebaseVisionTextDetector detector = FirebaseVision.getInstance().getVisionTextDetector();
        detector.detectInImage(image).addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
            @Override
            public void onSuccess(FirebaseVisionText firebaseVisionText) {
                processTextRecognitionResult(firebaseVisionText);
            }
        });
    }
//THIS FUNCTION IS USED TO PARSE TEXT FROM THE FirebaseVisionText OBJECT

    @SuppressLint("SetTextI18n")
    private void processTextRecognitionResult(FirebaseVisionText texts) {
        List<FirebaseVisionText.Block> blocks = texts.getBlocks();
        if (blocks.size() == 0) {
           // text.setText("No text found!");
            mChatMEssageView.setHint("No Text Found");
            return;
        }
        StringBuilder sb = new StringBuilder("");
        for (int i = 0; i < blocks.size(); i++) {
            List<FirebaseVisionText.Line> lines = blocks.get(i).getLines();
            for (int j = 0; j < lines.size(); j++) {
                List<FirebaseVisionText.Element> elements = lines.get(j).getElements();
                for (int k = 0; k < elements.size(); k++) sb.append(elements.get(k).getText()).append(" ");
                sb.append("\n");
            }
            sb.append("\n");
        }
       // text.setText(sb.toString());
        mChatMEssageView.setText(sb.toString());
    } */

  /*  public void sendNotification(){

        Gson gson = new Gson();
        ContactsContract.RawContacts.Data data = new ContactsContract.RawContacts.Data();
        data.setTitle("Enter your message here");
        PostRequestData postRequestData = new PostRequestData();
        postRequestData.setTo("Enter receiver FCM token here");
        postRequestData.setData(data);
        String json = gson.toJson(postRequestData);
        String url = "https://fcm.googleapis.com/fcm/send";

        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        OkHttpClient client = new OkHttpClient();


        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .header("Authorization", "key=YOUR SERVER KEY")
                .post(body)
                .build();


        Callback responseCallBack = new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.v("Fail Message", "fail");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.v("response", response.toString());
            }


        };
        Call call = client.newCall(request);
        call.enqueue(responseCallBack);

    }  */

  /*public void sendMessageNotification(String title, String message, String token, String type){

      String append = "title=" + title + "&message=" + message + "&image_url=" + type + "&action_destination=" + mCurrentUserId + "&firebase_token=" + token;

      String url = "http://thunderworm.esy.es/circle/notifications/send.php?" + append;

      Map<String, String> params = new HashMap<String, String>();


      jsObjRequest = new CustomRequest(Request.Method.GET, url, params, new Response.Listener<String>() {

          @Override
          public void onResponse(String response) {
              try {
                  Log.d("Response: ", response);


              } catch (Exception e) {
                  e.printStackTrace();
              }

          }
      }, new Response.ErrorListener() {

          @Override
          public void onErrorResponse(VolleyError response) {
              Log.d("Response: ", response.toString());
              Toast.makeText(ChatActivity.this,"Connectivity Error!! Please Try Again",Toast.LENGTH_SHORT).show();
              finish();

          }
      });

      CircleMessenger.getInstance().addToRequestQueue(jsObjRequest);


  }  */

    @Override
    public void onBackPressed() {
        if(chatBottomSheet.isSheetShowing()){
            chatBottomSheet.dismissSheet();
        } else{
          //  supportFinishAfterTransition();
            super.onBackPressed();
        }

    }

    public static String getPathFromUri(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


}
