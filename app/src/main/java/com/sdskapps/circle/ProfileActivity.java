package com.sdskapps.circle;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProfileActivity extends AppCompatActivity {

    private ImageView mProfileImage;
    private TextView mProfileName, mProfileStatus, mProfileFriendsCount;
    private Button mProfileReqBtn, mDeclineBtn;

    private DatabaseReference mUsersDatabase;
    private DatabaseReference mFriendReqDatabase;
    private DatabaseReference mFriendDatabase;
    private DatabaseReference mNotificationDatabse;
    private DatabaseReference mRootRef;
    private FirebaseUser mCurrentUser;

    private ProgressDialog mProgressDialog;
    private ProgressBar mImageProgress;

    private String mCurrent_state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Window w = getWindow(); // in Activity's onCreate() for instance
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        final String user_id= getIntent().getStringExtra("user_id");
        mRootRef=FirebaseDatabase.getInstance().getReference();

        mUsersDatabase= FirebaseDatabase.getInstance().getReference().child("Users").child(user_id);
        mFriendReqDatabase=FirebaseDatabase.getInstance().getReference().child("Friend_req");
        mFriendDatabase=FirebaseDatabase.getInstance().getReference().child("Friends");
        mNotificationDatabse=FirebaseDatabase.getInstance().getReference().child("notifications");
        mCurrentUser= FirebaseAuth.getInstance().getCurrentUser();

        mProfileName=(TextView) findViewById(R.id.profile_displayName);
        mProfileStatus=(TextView) findViewById(R.id.profile_status);
        mProfileFriendsCount=(TextView) findViewById(R.id.profile_totalFirends);
        mProfileImage=(ImageView) findViewById(R.id.profile_image);
        mProfileReqBtn=(Button) findViewById(R.id.profile_send_req_btn);
        mDeclineBtn =(Button) findViewById(R.id.profile_decline_btn);
        mImageProgress = (ProgressBar) findViewById(R.id.profileImageProgressLoading);
        mDeclineBtn.setVisibility(View.INVISIBLE);
        mDeclineBtn.setEnabled(false);

        mCurrent_state="not_friends";


        mProgressDialog=new ProgressDialog(this);
        mProgressDialog.setTitle("Loading User Data");
        mProgressDialog.setMessage("Please wait while we load user data.");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        mUsersDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String name=dataSnapshot.child("name").getValue().toString();
                String status=dataSnapshot.child("status").getValue().toString();
                String image=dataSnapshot.child("image").getValue().toString();

                mProfileName.setText(name);
                mProfileStatus.setText(status);

                Picasso.with(ProfileActivity.this).load(image).networkPolicy(NetworkPolicy.OFFLINE)
                        .placeholder(R.drawable.default_avatar1).into(mProfileImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        mImageProgress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        Picasso.with(ProfileActivity.this).load(image).placeholder(R.drawable.default_avatar1).into(mProfileImage, new Callback() {
                            @Override
                            public void onSuccess() {
                                mImageProgress.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                mImageProgress.setVisibility(View.GONE);
                            }
                        });
                    }
                });

                //----------------------FRIENDS LIST / REQUEST FEATURE----------------

                mFriendReqDatabase.child(mCurrentUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot Snapshot) {

                        if(Snapshot.hasChild(user_id)){

                            String req_type = Snapshot.child(user_id).child("request_type").getValue().toString();

                            if(req_type.equals("received")){
                                mCurrent_state="req_received";
                                mProfileReqBtn.setText("Accept Friend Request");

                                mDeclineBtn.setVisibility(View.VISIBLE);
                                mDeclineBtn.setEnabled(true);

                            } else if(req_type.equals("sent")){
                                mCurrent_state="req_sent";
                                mProfileReqBtn.setText("Cancel Friend Request");

                                mDeclineBtn.setVisibility(View.INVISIBLE);
                                mDeclineBtn.setEnabled(false);

                            } mProgressDialog.dismiss();

                        } else {
                            mFriendDatabase.child(mCurrentUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot frienddataSnapshot) {

                                    if(frienddataSnapshot.hasChild(user_id)){

                                        mCurrent_state="friends";
                                        mProfileReqBtn.setText("Unfriend this person");

                                        mDeclineBtn.setVisibility(View.INVISIBLE);
                                        mDeclineBtn.setEnabled(false);

                                    }
                                    mProgressDialog.dismiss();

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                    mProgressDialog.dismiss();

                                }
                            });
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mProfileReqBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // -----------------Not Firends State------------------
                mProfileReqBtn.setEnabled(false);

                if(mCurrent_state.equals("not_friends")){

                    DatabaseReference newNotificationRef=mRootRef.child("notifications").child(user_id).push();
                    String newNotificationId=newNotificationRef.getKey();

                    HashMap<String,String> notificationData=new HashMap<String, String>();
                    notificationData.put("from",mCurrentUser.getUid());
                    notificationData.put("type","request");
                    Map requestMap=new HashMap();
                    requestMap.put("Friend_req/" + mCurrentUser.getUid() + "/" + user_id + "/request_type", "sent");
                    requestMap.put("Friend_req/" + user_id + "/" + mCurrentUser.getUid() + "/request_type", "received");
                    requestMap.put("notifications/" + user_id + "/" + newNotificationId, notificationData);

                    mRootRef.updateChildren(requestMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            if(databaseError!=null){

                                Toast.makeText(ProfileActivity.this, "There was some error in sending the friend request",Toast.LENGTH_LONG).show();

                            }
                            mProfileReqBtn.setEnabled(true);
                            mCurrent_state = "req_sent";
                            mProfileReqBtn.setText("Cancel Friend Request");

                        }
                    });
                }

                // -----------------Cancel Firends State------------------

                if(mCurrent_state.equals("req_sent")) {

                    mFriendReqDatabase.child(mCurrentUser.getUid()).child(user_id).removeValue()
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    mFriendReqDatabase.child(user_id).child(mCurrentUser.getUid()).removeValue()
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    mProfileReqBtn.setEnabled(true);
                                                    mCurrent_state = "not_friends";
                                                    mProfileReqBtn.setText("Send Friend Request");
                                                    mDeclineBtn.setVisibility(View.INVISIBLE);
                                                    mDeclineBtn.setEnabled(false);

                                                }
                                            });

                                }
                            });

                }
                    // -----------REQ RECEIVED STATE-----------------
                    if(mCurrent_state.equals("req_received")){

                        final String currentDate= DateFormat.getDateTimeInstance().format(new Date());
                        Map friendsMap=new HashMap();
                        friendsMap.put("Friends/" + mCurrentUser.getUid() + "/" + user_id+ "/date", currentDate);
                        friendsMap.put("Friends/" + user_id + "/" + mCurrentUser.getUid()+ "/date", currentDate);

                        friendsMap.put("Friend_req/" + mCurrentUser.getUid() + "/" + user_id, null);
                        friendsMap.put("Friend_req/" +user_id + "/" + mCurrentUser.getUid(), null);

                        mRootRef.updateChildren(friendsMap, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                if(databaseError == null){
                                    mProfileReqBtn.setEnabled(true);
                                    mCurrent_state="friends";
                                    mProfileReqBtn.setText("Unfriend this person");

                                    mDeclineBtn.setVisibility(View.INVISIBLE);
                                    mDeclineBtn.setEnabled(false);
                                } else{
                                    String error=databaseError.getMessage();
                                    Toast.makeText(ProfileActivity.this, error, Toast.LENGTH_LONG).show();
                                }

                            }
                        });
                      /*  mFriendDatabase.child(mCurrentUser.getUid()).child(user_id).setValue(currentDate).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                                mFriendDatabase.child(user_id).child(mCurrentUser.getUid()).setValue(currentDate)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {

                                                mFriendReqDatabase.child(mCurrentUser.getUid()).child(user_id).removeValue()
                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {

                                                                mFriendReqDatabase.child(user_id).child(mCurrentUser.getUid()).removeValue()
                                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                            @Override
                                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                                mProfileReqBtn.setEnabled(true);
                                                                                mCurrent_state="friends";
                                                                                mProfileReqBtn.setText("Unfriend this person");

                                                                                mDeclineBtn.setVisibility(View.INVISIBLE);
                                                                                mDeclineBtn.setEnabled(false);
                                                                            }
                                                                        });

                                                            }
                                                        });

                                            }
                                        });

                            }
                        });
*/

                }

                //--------UNFIREND-------------


               if(mCurrent_state.equals("friends")){

                   Map unfriendMap= new HashMap();
                   unfriendMap.put("Friends/" + mCurrentUser.getUid() + "/" + user_id+ "/date", null);
                   unfriendMap.put("Friends/" + user_id + "/" + mCurrentUser.getUid()+ "/date", null);

                   mRootRef.updateChildren(unfriendMap, new DatabaseReference.CompletionListener() {
                       @Override
                       public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                           if(databaseError == null){
                               mProfileReqBtn.setEnabled(true);
                               mCurrent_state="not_friends";
                               mProfileReqBtn.setText("Send Friend Request");

                               mDeclineBtn.setVisibility(View.INVISIBLE);
                               mDeclineBtn.setEnabled(false);
                           } else{
                               String error=databaseError.getMessage();
                               Toast.makeText(ProfileActivity.this, error, Toast.LENGTH_LONG).show();
                           }

                       }
                   });

               }


                 /*        mFriendDatabase.child(mCurrentUser.getUid()).child(user_id).removeValue()
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                        mFriendDatabase.child(user_id).child(mCurrentUser.getUid()).removeValue()
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        mProfileReqBtn.setEnabled(true);
                                                        mCurrent_state = "not_friends";
                                                        mProfileReqBtn.setText("Send Friend Request");
                                                    }
                                                });

                                    }
                                });


                } */
            }
        });

    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
