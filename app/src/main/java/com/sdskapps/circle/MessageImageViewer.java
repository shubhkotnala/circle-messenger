package com.sdskapps.circle;

import android.app.DownloadManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.sdskapps.circle.CircleRectTransition.CircleRectView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class MessageImageViewer extends AppCompatActivity {

    private CircleRectView messageImage;
    private ProgressBar mProgress;
    private FloatingActionButton btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_image_viewer);

        mProgress = (ProgressBar) findViewById(R.id.imageProgressLoading);

    //    mProgress.setVisibility(View.VISIBLE);

        messageImage = (CircleRectView) findViewById(R.id.settings_image);
        btnSave = (FloatingActionButton) findViewById(R.id.btn_imageDownload);

        final String uri = getIntent().getStringExtra("messageImage");

        Picasso.with(getApplicationContext()).load(uri).networkPolicy(NetworkPolicy.OFFLINE)
                .placeholder(R.drawable.circle_img).into(messageImage, new Callback() {
            @Override
            public void onSuccess() {
                mProgress.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                Picasso.with(getApplicationContext()).load(uri).placeholder(R.drawable.circle_img).into(messageImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        mProgress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        mProgress.setVisibility(View.GONE);
                    }
                });

            }
        });


        btnSave.setOnClickListener(v -> {
            Picasso.with(getApplicationContext()).load(uri).into(getTarget(uri));
          //  downloadFile(getApplicationContext(),"test","jpg",uri);
        });


    }

    private  Target getTarget(final String url){
        Target target = new Target(){

            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {

                    @Override
                    public void run() {

                        String path = Environment.getExternalStorageDirectory() + "/" + "Circle" + "/" + "Circle Images" + "/";
                        File dir = new File(path);
                        if(!dir.exists()){
                            dir.mkdirs();
                        }

                            String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                            StringBuilder salt = new StringBuilder();
                            Random rnd = new Random();
                            while (salt.length() < 18) { // length of the random string.
                                int index = (int) (rnd.nextFloat() * SALTCHARS.length());
                                salt.append(SALTCHARS.charAt(index));
                            }
                            String saltStr = salt.toString();

                        String fileName = path  + saltStr + ".jpg";

                        File file = new File(fileName);

                            try {
                                file.createNewFile();
                                FileOutputStream ostream = new FileOutputStream(file);
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
                                ostream.flush();
                                ostream.close();
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(MessageImageViewer.this, "Saved to Gallery", Toast.LENGTH_SHORT).show();
                                    }
                                });
//
                            } catch (IOException e) {
                                Log.e("IOException", e.getLocalizedMessage());
                            }


                    }
                }).start();

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        return target;
    }

    public void downloadFile(Context context, String fileName, String fileExtension, String url) {

        String path = Environment.getExternalStorageDirectory() + "/" + "Circle" + "/" + "Circle Images" + "/";
        File dir = new File(path);
        if(!dir.exists()){
            dir.mkdirs();
        }

        DownloadManager downloadmanager = (DownloadManager) context.
                getSystemService(Context.DOWNLOAD_SERVICE);
        Uri uri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(uri);

        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        //  request.setDestinationInExternalFilesDir(context, destinationDirectory, fileName + fileExtension);
       // request.setDestinationInExternalFilesDir(context,"/Circle/Circle Images",fileName + "." + fileExtension);
        request.setDestinationInExternalPublicDir("/Circle/Circle Images",fileName + "." + fileExtension);
        downloadmanager.enqueue(request);

        //  return downloadmanager.enqueue(request);
    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        supportFinishAfterTransition();
    }
}
